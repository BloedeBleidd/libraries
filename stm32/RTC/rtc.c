/*
 * rtc.c
 *
 *  Created on: Nov 3, 2016
 *      Author: BloedeBleidd
 */

#include "rtc.h"

void rtc_enter_to_config_mode(void);
void rtc_exit_from_config_mode(void);


#define CRYSTAL	(32768)
const uint16_t crystalFreq = CRYSTAL;

#if RTC_SECOND_INTERRUPT_ENABLE == 1
volatile uint8_t rtc_second_flag;
#endif

uint8_t rtc_init (SystemTime_t * time)
{
	uint8_t result=0;

	RCC->APB1ENR = RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN;
	PWR->CR |= PWR_CR_DBP;

	RCC->BDCR = RCC_BDCR_BDRST;
	RCC->BDCR = RCC_BDCR_LSEON;
	while ( (RCC->BDCR & RCC_BDCR_LSERDY) == 0 );

	RCC->BDCR |= RCC_BDCR_RTCSEL_LSE | RCC_BDCR_RTCEN;
	RTC->CRL &= ~RTC_CRL_RSF;
	while ( (RTC->CRL & RTC_CRL_RSF) == 0 );

	rtc_enter_to_config_mode();

	#if RTC_SECOND_INTERRUPT_ENABLE == 1
	RTC->CRH = RTC_CRH_SECIE;
	#endif
	RTC->PRLH = 0;
	RTC->PRLL = crystalFreq-1;

	if(rtc_set_time(time))	result=1;

	rtc_exit_from_config_mode();

	#if RTC_SECOND_INTERRUPT_ENABLE == 1
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_EnableIRQ(RTC_IRQn);
	#endif

	return result;
}

void rtc_enter_to_config_mode(void)
{
	while ( (RTC->CRL & RTC_CRL_RTOFF) == 0 );
	RTC->CRL |= RTC_CRL_CNF;
}

void rtc_exit_from_config_mode(void)
{
	RTC->CRL &= (uint16_t)~((uint16_t)RTC_CRL_CNF);
	while ( (RTC->CRL & RTC_CRL_RTOFF) == 0 );
}

uint32_t rtc_get_counter(void)
{
	uint16_t high1 = 0, high2 = 0, low = 0;

	high1 = RTC->CNTH;
	low   = RTC->CNTL;
	high2 = RTC->CNTH;

	if (high1 != high2)
	{
		/* In this case the counter roll over during reading of CNTL and CNTH registers,
		read again CNTL register then return the counter value */
		return (((uint32_t) high2 << 16 ) | RTC->CNTL);
	}
	else
	{	/* No counter roll over during reading of CNTL and CNTH registers, counter
		value is equal to first value of CNTL and CNTH */
		return (((uint32_t) high1 << 16 ) | low);
	}
}

void rtc_set_counter(uint32_t counterValue)
{
	rtc_enter_to_config_mode();
	/* Set RTC COUNTER MSB word */
	RTC->CNTH = counterValue >> 16;
	/* Set RTC COUNTER LSB word */
	RTC->CNTL = (counterValue & ((uint32_t)0x0000FFFF));
	rtc_exit_from_config_mode();
}

void rtc_get_time(SystemTime_t * time)
{
	uint32_t counterValue;
	counterValue = rtc_get_counter();
	time->timestamp = counterValue;
	calendar_timestamp_to_date(counterValue,time);
}

uint8_t rtc_set_time(SystemTime_t * time)
{
	if(time->second>=60 || time->minute>=60 || time->hour>=24 || time->day>=31 || time->month>=12)	return 0;

	uint32_t counterValue;
	counterValue = calendar_date_to_timestamp(time);
	rtc_set_counter(counterValue);

	return 1;
}


#if RTC_SECOND_INTERRUPT_ENABLE == 1
void RTC_IRQHandler(void)
{
	if ( (RTC->CRL & RTC_CRL_SECF) == 1 )
	{
		RTC->CRL &= ~RTC_CRL_SECF;
		rtc_second_flag = 1;
	}
}
#else
uint8_t rtc_second_flag(void)
{
	if((RTC->CRL & RTC_CRL_SECF))
	{
		RTC->CRL &= ~RTC_CRL_SECF;
		return 1;
	}
	else	return 0;
}
#endif

char rtc_time_string[9] ;

char* rtc_get_time_string(SystemTime_t * time)
{
	char bufH[3],bufM[3],bufS[3];

	long_to_ascii(time->hour, bufH, decimal);
	long_to_ascii(time->minute, bufM, decimal);
	long_to_ascii(time->second, bufS, decimal);

	if(time->hour/10==0)
	{
		strcpy(rtc_time_string,"0");
		strcat(rtc_time_string,bufH);
	}
	else
	{
		strcpy(rtc_time_string,bufH);
	}
	strcat(rtc_time_string,":");
	if(time->minute/10==0)	strcat(rtc_time_string,"0");;
	strcat(rtc_time_string,bufM);
	strcat(rtc_time_string,":");
	if(time->second/10==0)	strcat(rtc_time_string,"0");;
	strcat(rtc_time_string,bufS);

	return rtc_time_string;
}

char rtc_date_string[11] ;

char* rtc_get_date_string(SystemTime_t * time)
{
	char bufD[3],bufM[3],bufY[5];

	long_to_ascii(time->day+1, bufD, decimal);
	long_to_ascii(time->month+1, bufM, decimal);
	long_to_ascii(time->year, bufY, decimal);

	if(time->day/10==0)
	{
		strcpy(rtc_date_string,"0");
		strcat(rtc_date_string,bufD);
	}
	else
	{
		strcpy(rtc_date_string,bufD);
	}
	strcat(rtc_date_string,".");
	if(time->month/10==0)	strcat(rtc_date_string,"0");;
	strcat(rtc_date_string,bufM);
	strcat(rtc_date_string,".");
	strcat(rtc_date_string,bufY);

	return rtc_date_string;
}
