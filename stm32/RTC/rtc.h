/*
 * rtc.h
 *
 *  Created on: Nov 3, 2016
 *      Author: BloedeBleidd
 */

#ifndef RTC_RTC_H_
#define RTC_RTC_H_

#include "stm32f10x.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "../LTOA/ltoa.h"

#define RTC_SECOND_INTERRUPT_ENABLE 0

typedef enum
{
	January   = 0,
	February  = 1,
	March     = 2,
	April     = 3,
	May       = 4,
	June      = 5,
	July      = 6,
	August    = 7,
	September = 8,
	October   = 9,
	November  = 10,
	December  = 11,
}Month_t;

typedef enum
{
	Sunday    = 0,
	Monday    = 1,
	Tuesday   = 2,
	Wednesday = 3,
	Thursday  = 4,
	Friday    = 5,
	Saturday  = 6,
}DayOfWeek_t;

typedef struct
{
	uint8_t second;        //!< 0-59
	uint8_t minute;        //!< 0-59
	uint8_t hour;          //!< 0-23
	uint8_t day;           //!< 0-30 \note First day of month is 0, not 1.
	uint8_t month;         //!< 0 January - 11 December
	uint16_t year;         //!< 1970-2105
	uint8_t dayofweek;     //!< 0 Sunday  - 6 Saturday
	uint32_t timestamp;
}SystemTime_t;

#if RTC_SECOND_INTERRUPT_ENABLE == 1
extern volatile uint8_t rtc_second_flag;
#else
uint8_t rtc_second_flag(void);
#endif

void calendar_timestamp_to_date(uint32_t,SystemTime_t*);
uint32_t calendar_date_to_timestamp(SystemTime_t*);

uint8_t rtc_init (SystemTime_t*);
void rtc_get_time(SystemTime_t*);
uint8_t rtc_set_time(SystemTime_t*);

uint32_t rtc_get_counter(void);
void rtc_set_counter(uint32_t);

char* rtc_get_time_string(SystemTime_t * time);
char* rtc_get_date_string(SystemTime_t * time);

#endif /* RTC_RTC_H_ */
