/*
 * calendar.c
 *
 *  Created on: Nov 4, 2016
 *      Author: BloedeBleidd
 */


#include "rtc.h"


//! Unix epoch year
#define EPOCH_YEAR 1970

//! Number of seconds in a day
#define SECS_PER_DAY 86400UL

//! Number of seconds in an hour
#define SECS_PER_HOUR 3600UL

//! Number of seconds in a minute
#define SECS_PER_MINUTE 60UL

//! Number of days in a specified month. Index 1 for leap year, else 0.
const uint8_t month[2][12] =
{
	{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
	{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
};


static bool calendar_leapyear(uint16_t year)
{
	if(!((year) % 4) && (((year) % 100) || !((year) % 400)))
	{
		return true;
	}
	else
	{
		return false;
	}
}


static uint16_t calendar_yearsize(uint16_t year)
{
	if (calendar_leapyear(year))
	{
		return 366;
	}
	else
	{
		return 365;
	}
}


static void calendar_add_year_to_date(SystemTime_t* time)
{
	if (time->year < 2105)
	{
		time->year++;
	}
}


static void calendar_add_month_to_date(SystemTime_t* time)
{
	uint8_t months = time->month;

	months++;

	if (months == 12)
	{
		months = 0;
		calendar_add_year_to_date(time);
	}

	time->month = months;
}


static void calendar_add_day_to_date(SystemTime_t* time)
{
	uint8_t date = time->day;
	uint8_t months = time->month;
	uint8_t year = time->year;

	date++;

	if (date == month[calendar_leapyear(year)][months])
	{
		date = 0;
		calendar_add_month_to_date(time);
	}

	time->dayofweek++;

	if (time->dayofweek == 7)
	{
		time->dayofweek = 0;
	}

	time->day = date;
}


static void calendar_add_hour_to_date(SystemTime_t* time)
{
	int8_t hour = time->hour;

	hour++;

	if (hour == 24)
	{
		hour = 0;
		calendar_add_day_to_date(time);
	}

	time->hour = hour;
}


/*static*/ void calendar_add_minute_to_date(SystemTime_t* time)
{
	uint8_t minute = time->minute;

	minute++;

	if (minute == 60)
	{
		minute = 0;
		calendar_add_hour_to_date(time);
	}

	time->minute = minute;
}


bool calendar_is_date_valid(SystemTime_t* time)
{
	// Make sure time is valid
	if ((time->second >= 60) || (time->minute >= 60) || (time->hour >= 24))
	{
		return false;
	}

	// Make sure month and date is valid
	if ((time->month >= 12) || (time->day >=31))
	{
		return false;
	}

	// Make sure days in month are not more than it should be
	if (time->day >= month[calendar_leapyear(time->year)][time->month])
	{
		return false;
	}

	// Make sure year is not earlier than 1970 and before 2106
	if ((time->year < EPOCH_YEAR) || (time->year >= 2106))
	{
		return false;
	}
	else
	{
		return true;
	}
}


void calendar_timestamp_to_date(uint32_t timestamp,SystemTime_t* time)
{
	uint32_t day_number;
	uint32_t day_clock;

	time->year = EPOCH_YEAR;
	time->month = 0;

	day_clock = timestamp % SECS_PER_DAY;
	day_number = timestamp / SECS_PER_DAY;

	time->second = day_clock % SECS_PER_MINUTE;
	time->minute = (day_clock % SECS_PER_HOUR) / SECS_PER_MINUTE;
	time->hour = day_clock / SECS_PER_HOUR;
	time->dayofweek = (day_number + 4) % 7;

	while (day_number >= calendar_yearsize(time->year))
	{
		day_number -= calendar_yearsize(time->year);
		time->year++;
	}

	while (day_number >= month[calendar_leapyear(time->year)][time->month])
	{
		day_number -= month[calendar_leapyear(time->year)][time->month];
		time->month++;
	}

	time->day = day_number;
}




uint32_t calendar_date_to_timestamp(SystemTime_t* time)
{

	// Make sure date is valid
	if (!calendar_is_date_valid(time))	return 0;

	uint32_t timestamp = 0;
	uint8_t date_month;
	uint16_t date_year;

	date_month = time->month;
	date_year = time->year;

	// Add number of seconds elapsed in current month
	timestamp += (time->day * SECS_PER_DAY) + (time->hour * SECS_PER_HOUR) + (time->minute * SECS_PER_MINUTE) + time->second;

	while (date_month != 0)
	{
		date_month--;
		// Add number of seconds in months of current year
		timestamp += month[calendar_leapyear(date_year)][date_month] * SECS_PER_DAY;
	}

	while (date_year > EPOCH_YEAR)
	{
		date_year--;
		// Add number of seconds in all years since epoch year
		timestamp += calendar_yearsize(date_year) * SECS_PER_DAY;
	}

	return timestamp;
}
