/*
 * sd_card_stm32f4config.h
 *
 *  Created on: Aug 31, 2016
 *      Author: BloedeBleidd
 */

#ifndef SD_CARD_STM32F4CONFIG_H_
#define SD_CARD_STM32F4CONFIG_H_

/****************************** Configuration ******************************/

#define SPIx 			SPI1
#define GPIOx 			GPIOA
#define CS_PIN			3
#define CLK_PIN			5
#define MISO_PIN		6
#define MOSI_PIN		7

#define SPI_PRESCALLER	0x00

/*
 * 2   == (0x00)
 * 4   == (SPI_CR1_BR_0)
 * 8   == (SPI_CR1_BR_1)
 * 16  == (SPI_CR1_BR_0 | SPI_CR1_BR_1)
 * 32  == (SPI_CR1_BR_2)
 * 64  == (SPI_CR1_BR_0 | SPI_CR1_BR_2)
 * 128 == (SPI_CR1_BR_1 | SPI_CR1_BR_2)
 * 256 == (SPI_CR1_BR)
 */

/**************************** End Configuration ****************************/



#define CS_HIGH 				GPIOx->BSRRL = (1<<CS_PIN)
#define CS_LOW 					GPIOx->BSRRH = (1<<CS_PIN)

#define FCLK_SLOW() { SPIx->CR1 = (SPIx->CR1 & ~0x38) | SPI_CR1_BR; }	/* Set SCLK = PCLK / 256 */
#define FCLK_FAST() { SPIx->CR1 = (SPIx->CR1 & ~0x38) | SPI_PRESCALLER; }

#define	SPI_DATA				SPIx->DR

#define	SPI_TXE_FLAG			SPIx->SR & SPI_SR_TXE
#define SPI_WAIT_FOR_TXE_FLAG	while(!(SPI_TXE_FLAG))

#define	SPI_RXE_FLAG			SPIx->SR & SPI_SR_RXNE
#define SPI_WAIT_FOR_RXE_FLAG	while(!(SPI_RXE_FLAG))

#define	SPI_BUSY_FLAG			SPIx->SR & SPI_SR_BSY
#define SPI_WAIT_FOR_BUSY_FLAG	while((SPI_BUSY_FLAG))

#define SPI_16BIT_MODE			SPIx->CR1 &= ~SPI_CR1_SPE; SPIx->CR1 |= (SPI_CR1_DFF | SPI_CR1_SPE);
#define SPI_8BIT_MODE			SPIx->CR1 &= ~(SPI_CR1_DFF | SPI_CR1_SPE); SPIx->CR1 |= SPI_CR1_SPE;

#endif /* SD_CARD_STM32F4CONFIG_H_ */
