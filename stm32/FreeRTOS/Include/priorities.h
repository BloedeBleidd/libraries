/*
 * priorities.h
 *
 *  Created on: 19.01.2017
 *      Author: BloedeBleidd
 */

#ifndef FREERTOS_INCLUDE_PRIORITIES_H_
#define FREERTOS_INCLUDE_PRIORITIES_H_


#define tskLOWEST_PRIORITY		1U
#define tskLOW_PRIORITY			2U
#define tskNORMAL_PRIORITY		3U
#define tskHIGH_PRIORITY		4U
#define tskHIGHEST_PRIORITY		5U


#endif /* FREERTOS_INCLUDE_PRIORITIES_H_ */
