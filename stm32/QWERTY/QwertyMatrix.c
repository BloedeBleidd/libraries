/*
 * QwertyMatrix.c
 *
 *  Created on: Nov 13, 2016
 *      Author: BloedeBleidd
 */

#include "QwertyMatrix.h"

const uint16_t pressTime=PRESS_TIME / 4, holdTime=HOLD_TIME / 4;

volatile qwertyKey_t qwertyKey;

void qwerty_init(void)
{
	gpio_pin_config(QWERTY_ROW_GPIOx, QWERTY_ROW_PIN1, gpio_mode_output_PP_50MHz);
	gpio_pin_config(QWERTY_ROW_GPIOx, QWERTY_ROW_PIN2, gpio_mode_output_PP_50MHz);
	gpio_pin_config(QWERTY_ROW_GPIOx, QWERTY_ROW_PIN3, gpio_mode_output_PP_50MHz);
	gpio_pin_config(QWERTY_ROW_GPIOx, QWERTY_ROW_PIN4, gpio_mode_output_PP_50MHz);

	gpio_pin_config(QWERTY_COLUMN_GPIOx, QWERTY_COLUMN_PIN1, gpio_mode_input_pull);
	gpio_pin_config(QWERTY_COLUMN_GPIOx, QWERTY_COLUMN_PIN2, gpio_mode_input_pull);
	gpio_pin_config(QWERTY_COLUMN_GPIOx, QWERTY_COLUMN_PIN3, gpio_mode_input_pull);
	gpio_pin_config(QWERTY_COLUMN_GPIOx, QWERTY_COLUMN_PIN4, gpio_mode_input_pull);
}

static void qwerty_key_check(uint8_t press, uint8_t row, uint8_t col)
{
	uint8_t position = 4*row+col;
	uint8_t changed = released;

	if(press)
	{
		qwertyKey.Timer[position]++;
		if(qwertyKey.Timer[position] >= holdTime)	{ changed = held_still; }
	}
	else
	{
		if(qwertyKey.Timer[position] >= pressTime)
		{
			if(qwertyKey.Timer[position] >= holdTime)	{ changed = held; }
			else										{ changed = pressed;}
		}
		qwertyKey.Timer[position] = 0;
	}

	if(changed != released)	{ qwertyKey.Status[position] = changed; }
}

static void qwerty_column_check(uint8_t row)
{
	qwerty_key_check(QWERTY_PRESS_COLUMN1, row, 0);
	qwerty_key_check(QWERTY_PRESS_COLUMN2, row, 1);
	qwerty_key_check(QWERTY_PRESS_COLUMN3, row, 2);
	qwerty_key_check(QWERTY_PRESS_COLUMN4, row, 3);
}

void qwerty_row_handling (void)
{
	static uint8_t row_step=0;

	if(row_step==0)
	{
		QWERTY_SET_ROW1;
		qwerty_column_check(0);
	}
	else if(row_step==1)
	{
		QWERTY_SET_ROW2;
		qwerty_column_check(1);
	}
	else if(row_step==2)
	{
		QWERTY_SET_ROW3;
		qwerty_column_check(2);
	}
	else if(row_step==3)
	{
		QWERTY_SET_ROW4;
		qwerty_column_check(3);
	}

	QWERTY_RESET_ROWS;

	if(++row_step>=4)	{ row_step=0; }
}

void qwerty_ClearKeyStatus(uint8_t nr)
{
	qwertyKey.Status[nr] = released;
}

void qwerty_ClearEveryKeyStatus(void)
{
	for(uint8_t i=0;i<16;i++)
	{
		qwertyKey.Status[i] = released;
	}
}

uint8_t qwerty_GetKeyStatus(uint8_t nr)
{
	return qwertyKey.Status[nr];
}

uint8_t qwerty_GetKeyStatusAndClear(uint8_t nr)
{
	uint8_t status = qwertyKey.Status[nr];

	if(status!=held_still)
	{
		qwertyKey.Status[nr] = released;
	}

	return status;
}

uint8_t qwerty_isKeyReleased(uint8_t nr)
{
	if(qwertyKey.Status[nr]==released)		return 1;
	else	return 0;
}

uint8_t qwerty_isKeyPressed(uint8_t nr)
{
	if(qwertyKey.Status[nr]==pressed)		return 1;
	else	return 0;
}

uint8_t qwerty_isKeyHeld(uint8_t nr)
{
	if(qwertyKey.Status[nr]==held)			return 1;
	else	return 0;
}

uint8_t qwerty_isKeyHeldStill(uint8_t nr)
{
	if(qwertyKey.Status[nr]==held_still)	return 1;
	else	return 0;
}











