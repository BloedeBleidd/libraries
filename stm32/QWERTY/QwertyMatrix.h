/*
 * QwertyMatrix.h
 *
 *  Created on: Nov 13, 2016
 *      Author: BloedeBleidd
 */

#ifndef QWERTY_QWERTYMATRIX_H_
#define QWERTY_QWERTYMATRIX_H_

#include "stm32f10x.h"
#include <stdint.h>
#include <stdlib.h>
#include "../GPIO/gpio.h"

/**************************************** Configuration ****************************************/

#define PRESS_TIME				50
#define HOLD_TIME				500

#define QWERTY_ROW_GPIOx		GPIOA
#define QWERTY_ROW_PIN1			Pin7
#define QWERTY_ROW_PIN2			Pin6
#define QWERTY_ROW_PIN3			Pin5
#define QWERTY_ROW_PIN4			Pin4

#define QWERTY_COLUMN_GPIOx		GPIOA
#define QWERTY_COLUMN_PIN1		Pin3
#define QWERTY_COLUMN_PIN2		Pin2
#define QWERTY_COLUMN_PIN3		Pin1
#define QWERTY_COLUMN_PIN4		Pin0

/**************************************** Configuration ****************************************/


#define QWERTY_ROW_ODRx			QWERTY_ROW_GPIOx->ODR
#define QWERTY_COLUMN_IDRx		QWERTY_COLUMN_GPIOx->IDR

#define QWERTY_RESET_ROWS 		QWERTY_ROW_ODRx &= ~( QWERTY_ROW_PIN1 | QWERTY_ROW_PIN2 | QWERTY_ROW_PIN3 | QWERTY_ROW_PIN4 );

#define QWERTY_SET_ROW1			QWERTY_ROW_ODRx |= QWERTY_ROW_PIN1;
#define QWERTY_SET_ROW2			QWERTY_ROW_ODRx |= QWERTY_ROW_PIN2;
#define QWERTY_SET_ROW3			QWERTY_ROW_ODRx |= QWERTY_ROW_PIN3;
#define QWERTY_SET_ROW4			QWERTY_ROW_ODRx |= QWERTY_ROW_PIN4;

#define QWERTY_PRESS_COLUMN1	QWERTY_COLUMN_IDRx & QWERTY_COLUMN_PIN1
#define QWERTY_PRESS_COLUMN2	QWERTY_COLUMN_IDRx & QWERTY_COLUMN_PIN2
#define QWERTY_PRESS_COLUMN3	QWERTY_COLUMN_IDRx & QWERTY_COLUMN_PIN3
#define QWERTY_PRESS_COLUMN4	QWERTY_COLUMN_IDRx & QWERTY_COLUMN_PIN4



enum keyStatusEnum
{
	released,pressed,held,held_still
};

typedef struct
{
	uint8_t Status[16];
	uint16_t Timer[16];
}qwertyKey_t;

extern volatile qwertyKey_t qwertyKey;


extern volatile uint8_t qwertyKeyStatus[16];
extern volatile uint16_t qwertyKeyTimer[16];


void qwerty_init(void);
void qwerty_row_handling (void);

void qwerty_ClearKeyStatus(uint8_t);
void qwerty_ClearEveryKeyStatus(void);

uint8_t qwerty_GetKeyStatus(uint8_t);
uint8_t qwerty_GetKeyStatusAndClear(uint8_t);

uint8_t qwerty_isKeyReleased(uint8_t);
uint8_t qwerty_isKeyPressed(uint8_t);
uint8_t qwerty_isKeyHeld(uint8_t);
uint8_t qwerty_isKeyHeldStill(uint8_t);

#endif /* QWERTY_QWERTYMATRIX_H_ */
