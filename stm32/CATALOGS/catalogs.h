/*
 * catalogs.h
 *
 *  Created on: Nov 19, 2016
 *      Author: BloedeBleidd
 */

#ifndef CATALOGS_CATALOGS_H_
#define CATALOGS_CATALOGS_H_

#include "stm32f10x.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "FatFs/ff.h"
#include "FatFs/diskio.h"

#include "SH1106/sh1106.h"
#include "SH1106/fonts.h"

#include "wav_player.h"

#define TYPE_UNKNOW	0
#define TYPE_DIR	1
#define TYPE_WAV	2
#define TYPE_TXT	3
#define TYPE_BMP	4
#define TYPE_JPG	5

#define GLOBAL_BUFFER_SIZE	8192

extern char pathString[];
extern uint8_t globalFileBuffer[];
uint8_t actualMode;

void fileViewerHandler (void);

void enterToDirectory(void);

void cutLastStringFromPath (char* filePath);

void catalogRedraw (void);


#endif /* CATALOGS_CATALOGS_H_ */
