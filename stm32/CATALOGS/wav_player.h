/*
 * wav_player.h
 *
 *  Created on: Nov 20, 2016
 *      Author: BloedeBleidd
 */

#ifndef CATALOGS_WAV_PLAYER_H_
#define CATALOGS_WAV_PLAYER_H_

#include "stm32f10x.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "FatFs/ff.h"
#include "FatFs/diskio.h"

#include "SH1106/sh1106.h"
#include "SH1106/fonts.h"

#include "TaskManager/taskmanagerIDs.h"

#include "catalogs.h"


#define SAMPLE_TIMER	TIM2

#define PWM_TIMER		TIM1
#define PWM_CH1 		TIM1->CCR1
#define PWM_CH2 		TIM1->CCR2
#define PWM_CH3 		TIM1->CCR3
#define PWM_CH4 		TIM1->CCR4

void playerHardwareInit (void);
void navigationOnWavPlayer (void);

#endif /* CATALOGS_WAV_PLAYER_H_ */
