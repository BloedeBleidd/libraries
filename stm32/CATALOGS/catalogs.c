/*
 * catalogs.c
 *
 *  Created on: Nov 19, 2016
 *      Author: BloedeBleidd
 */


#include "catalogs.h"


/**************************************** typedefs ****************************************/


typedef struct
{
	uint16_t files;
	uint16_t offset;
	uint16_t cursor;
}Scroll;


typedef struct
{
	uint8_t type;
	char name[13];
}Data;

struct node
{
	Data data;
	struct node *next;
};


/**************************************** global variables ****************************************/


struct node *head = NULL;
Scroll displayData = {.files = 0, .offset = 0, .cursor = 0};
uint8_t actualMode = TYPE_DIR;
uint8_t globalFileBuffer[GLOBAL_BUFFER_SIZE];
char pathString[250] = {""};


/**************************************** functions declarations ****************************************/


struct node* listAppend(Data *data);
void listAppendWithSorting(struct node **head,Data *data);
void listDelete(struct node **head);
void listDisplay(struct node *head);
struct node* getListElement (struct node *head, uint16_t offset);
FRESULT scanFiles (char* path);
uint8_t getFileType (char* path);
void addStringToPath (char* filePath, char* string);
void cutLastStringFromPath (char* filePath);
void catalogDisplay(struct node *head);
void navigationOnCatalogs (void);


/**************************************** list ****************************************/


struct node* listAppend(Data *data)
{
	struct node *element;

	element = (struct node*)calloc(1,sizeof(struct node));
	element->data.type = data->type;
	element->next=NULL;
	strcpy(element->data.name , data->name);
	return element;
}

void listAppendWithSorting(struct node **head,Data *data)
{
	struct node *pom, *tmp = listAppend(data);

	if (*head == NULL)
	{
		*head = tmp;
	}
	else if (strcmp((*head)->data.name, tmp->data.name) > 0)
	{
		tmp->next = *head;
		*head = tmp;
	}
	else
	{
		pom = (*head);
		while ((pom->next != NULL) && (strcmp(pom->next->data.name, tmp->data.name) < 0))
		{
			pom = pom->next;
		}
		tmp->next = pom->next;
		pom->next = tmp;
	}
}

void listDelete(struct node **head)
{
	struct node *tmp;

	while (*head != NULL)
	{
		tmp = *head;
		*head = (*head)->next;
		free(tmp);
	}
}

struct node* getListElement (struct node *head, uint16_t offset)
{
	uint16_t i=0;
	struct node *tmp;
	tmp = head;

	while (tmp != NULL && i<offset)
	{
		tmp = tmp->next;
		i++;
	}
	return tmp;
}


/**************************************** path string ****************************************/


void addStringToPath (char* filePath, char* string)
{
	strcat(filePath, "/");
	strcat(filePath, string);
}

void cutLastStringFromPath (char* filePath)
{
	if(filePath[0])
	{
		char *ret;
		ret = strrchr( filePath, '/' );

		for(uint8_t i=0;i<13;i++)
		{
			ret[i] = 0;
		}
	}
}


/**************************************** files ****************************************/


FRESULT scanFiles (char* path)        // Start node to be scanned
{
	FRESULT res;
	DIR dir;
	FILINFO fno;
	UINT i=0;

	if(disk_initialize(0)) return FR_NOT_READY;

	res = f_opendir(&dir, path);                       /* Open the directory */

	if (res == FR_OK)
	{
		listDelete(&head);

		for(;;)
		{
			Data data;

			res = f_readdir(&dir, &fno);                   /* Read a directory item */
			if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */

			if (fno.fattrib & AM_DIR)	/* It is a directory */
			{
				data.type = TYPE_DIR;
			}
			else	/* It is a file. */
			{
				data.type = getFileType(fno.fname);
			}
			strcpy(data.name,fno.fname);
			listAppendWithSorting(&head,&data);
			i++;
		}
		f_closedir(&dir);
		displayData.files = i;
	}

	return res;
}

uint8_t getFileType (char* path)
{
	uint8_t fileType;

	if(strstr( path, ".TXT" ))
	{
		fileType = TYPE_TXT;
	}
	else if(strstr( path, ".WAV" ))
	{
		fileType = TYPE_WAV;
	}
	else if(strstr( path, ".BMP" ))
	{
		fileType = TYPE_BMP;
	}
	else if(strstr( path, ".JPG" ))
	{
		fileType = TYPE_JPG;
	}
	else
	{
		fileType = TYPE_UNKNOW;
	}

	return fileType;
}

void enterToDirectory (void)
{
	if(displayData.cursor==0)
	{
		cutLastStringFromPath(pathString);
		actualMode = TYPE_DIR;
	}
	else
	{
		struct node *tmp;
		tmp = getListElement(head,displayData.cursor-1);

		if(tmp->data.type==TYPE_DIR)
		{
			addStringToPath(pathString,tmp->data.name);
			displayData.cursor=0;
			displayData.offset=0;
			actualMode = TYPE_DIR;
		}
		else
		{
			switch(tmp->data.type)
			{
			case TYPE_WAV:
				addStringToPath(pathString,tmp->data.name);
				actualMode = TYPE_WAV;
				TaskManager_Edit_TaskStatus(ID_WAV_PLAYER,AWAIT);
			break;

			default:
				actualMode = TYPE_DIR;
			break;
			}
		}
	}

	catalogRedraw();
}


/**************************************** drawing ****************************************/


void catalogRedraw (void)
{
	scanFiles(pathString);
	catalogDisplay(head);
}


void catalogDisplay(struct node *head)
{
	uint8_t i=1,cursorPosition;
	struct node *tmp;

	cursorPosition = (displayData.cursor - displayData.offset) * 10;
	sh1106_draw_bufor_rect(0,0,89,60,black);
	sh1106_draw_bufor_rect(0,cursorPosition,4,8,white);

	sh1106_draw_bufor_string(5,0,"../Back",&Font_7x10,white,transparent);

	tmp = getListElement(head,displayData.offset);

	while (tmp != NULL)
	{
		sh1106_draw_bufor_string(5,10*i,tmp->data.name,&Font_7x10,white,transparent);
		tmp = tmp->next;
		i++;
		if(i>=6) break;
	}
	sh1106_refresh_display();
}


/**************************************** catalog navigation ****************************************/

void navigationOnCatalogs (void)
{
	if(qwerty_GetKeyStatusAndClear(1)==pressed)	//up
	{
		if(displayData.cursor>0)
		{
			displayData.cursor--;
		}
		if( (displayData.cursor > 2) && ((displayData.cursor+2) < displayData.files))
		{
			displayData.offset--;
		}

		catalogDisplay(head);
	}

	if(qwerty_GetKeyStatusAndClear(5)==pressed)	//ok
	{
		enterToDirectory();
	}

	if(qwerty_GetKeyStatusAndClear(9)==pressed)	//down
	{
		if(displayData.cursor<displayData.files)
		{
			displayData.cursor++;
		}
		if( (displayData.cursor > 3) && ((displayData.offset+5) < displayData.files))
		{
			displayData.offset++;
		}
		catalogDisplay(head);
	}
}


/**************************************** handler ****************************************/


void fileViewerHandler (void)
{
	switch(actualMode)
	{
	case TYPE_DIR:
		navigationOnCatalogs();
	break;

	case TYPE_WAV:
		navigationOnWavPlayer();
	break;

	default:
	break;
	}
}










