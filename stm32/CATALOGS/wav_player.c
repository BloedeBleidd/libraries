/*
 * wav_player.c
 *
 *  Created on: Nov 20, 2016
 *      Author: BloedeBleidd
 */


#include "wav_player.h"


/**************************************** typedefs ****************************************/


enum
{
	disable=0,enable=1,pause=0,play=1,reset=0,set=1,off=0,on=1,lseekUpdate=2,
};


typedef struct
{
	uint8_t channels;
	uint8_t resolution;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint32_t dataSize;
	uint32_t secondsPlayed;
	uint32_t secondsAmount;
}wav_description_t;

typedef struct
{
	uint8_t playPause;		//0-pause 1-play
	uint8_t continueFlag;
	uint8_t switcherFlag;
	uint8_t scrollingFlag;
}playerStatus_t;

enum
{
	unknow,res8ch1,res8ch2,res16ch1,res16ch2,
};


/**************************************** global variables ****************************************/


volatile uint8_t resol=5;

volatile uint8_t soundMode;
volatile FSIZE_t offsetData;
volatile uint32_t scrollSpeed;
uint32_t byteRead;


playerStatus_t status= {.continueFlag=reset,.playPause=pause,.switcherFlag=reset};
wav_description_t description;


/**************************************** functions declarations ****************************************/


void playerPlaySong (void);
void playerDisplay(void);


/**************************************** hardware control ****************************************/


void playerSampleTimerControl (uint8_t control)
{
	if(control)
	{
		SAMPLE_TIMER->CR1 |= TIM_CR1_CEN;
	}
	else
	{
		SAMPLE_TIMER->CR1 &= ~TIM_CR1_CEN;
	}
}

void playerPwmControl (uint8_t control)
{
	if(control)
	{
		PWM_TIMER->CR1 |= TIM_CR1_CEN;
	}
	else
	{
		PWM_TIMER->CR1 &= ~TIM_CR1_CEN;
	}
}

void playerPlayPauseControl (uint8_t control)
{
	if(control)
	{
//		NVIC_EnableIRQ(TIM2_IRQn);
//		playerPwmControl(on);
		playerSampleTimerControl(on);
	}
	else
	{
//		NVIC_DisableIRQ(TIM2_IRQn);
//		playerPwmControl(off);
		playerSampleTimerControl(off);
		PWM_CH2 = 128;
		PWM_CH4 = 128;
		PWM_CH1 = 128;
		PWM_CH3 = 128;
	}
}

void playerSampleSpeedCalculation (void)
{
	SAMPLE_TIMER->ARR = (uint16_t)(((uint32_t)72000000 / description.sampleRate)-1);
}

void playerHardwareInit (void)
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	gpio_pin_config(GPIOA,Pin8,gpio_mode_alternate_PP_50MHz);
	gpio_pin_config(GPIOA,Pin9,gpio_mode_alternate_PP_50MHz);
	gpio_pin_config(GPIOA,Pin10,gpio_mode_alternate_PP_50MHz);
	gpio_pin_config(GPIOA,Pin11,gpio_mode_alternate_PP_50MHz);

	PWM_TIMER->CCMR1 = TIM_CCMR1_OC1PE | TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC2PE | TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1;
	PWM_TIMER->CCMR2 = TIM_CCMR2_OC3PE | TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC4PE | TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1;
	PWM_TIMER->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E;
	PWM_TIMER->BDTR = TIM_BDTR_MOE;
	PWM_TIMER->ARR = 255;
	PWM_TIMER->EGR = TIM_EGR_UG;
	PWM_TIMER->CR1 = TIM_CR1_ARPE;
	playerPwmControl(on);

	SAMPLE_TIMER->DIER = TIM_DIER_UIE;
	playerSampleTimerControl(off);
	NVIC_EnableIRQ(TIM2_IRQn);

	TaskManager_Replace_Add_Task(ID_WAV_PLAYER, PRIOR_WAV_PLAYER, playerPlaySong, 1);
	TaskManager_Edit_TaskStatus(ID_WAV_PLAYER,BLOCKED);
}


/**************************************** description etc ****************************************/


void playerGetScrollSPeed (void)
{
	uint8_t moduloSpeed=1;
	const uint16_t divider = 10000;

	scrollSpeed = description.dataSize / divider;

	if(soundMode==res16ch2)
	{
		moduloSpeed = scrollSpeed % 4;
	}
	else if(soundMode==res8ch2 || soundMode==res16ch1)
	{
		moduloSpeed = scrollSpeed % 2;
	}

	scrollSpeed -= moduloSpeed;
}


void playerGetAmountOfSeconds (void)
{
	description.secondsAmount = description.dataSize / description.byteRate;
}

bool playerGetPlayedSeconds (void)
{
	static uint32_t secondsPlayedBuffer=999999;

	description.secondsPlayed = ( (description.secondsAmount * (byteRead/1000) ) / (description.dataSize/1000) );

	if(description.secondsPlayed != secondsPlayedBuffer)
	{
		secondsPlayedBuffer = description.secondsPlayed;
		return true;
	}
	else	return false;

}


void playerGetWavDescription (FIL* fil)
{
	UINT br;
	uint8_t dataSizeBuf[4];

	f_lseek(fil,22);
	f_read(fil,&description.channels, 1, &br);

	f_lseek(fil,34);
	f_read(fil,&description.resolution, 1, &br);

	f_lseek(fil,24);
	f_read(fil,&description.sampleRate, 4, &br);

	f_lseek(fil,28);
	f_read(fil,&description.byteRate, 4, &br);

	f_lseek(fil,0x4c);
	f_read(fil,dataSizeBuf, 4, &br);

	description.dataSize = dataSizeBuf[0] | (dataSizeBuf[1]<<8) | (dataSizeBuf[2]<<16) | (dataSizeBuf[3]<<24);

	playerGetAmountOfSeconds();
}

uint8_t playerGetSoundMode (void)
{
	uint8_t mode;

	if(description.resolution==16)
	{
		if(description.channels==2)	mode=res16ch2;
		else						mode=res16ch1;
	}
	else if(description.resolution==8)
	{
		if(description.channels==2)	mode=res8ch2;
		else						mode=res8ch1;
	}
	else
	{
		mode = unknow;
	}

	return mode;
}


char* playerGetSongName (char* path)
{
	char *ret;
	ret = strrchr( path, '/' )+1;
	return ret;
}


/**************************************** display ****************************************/


char secondsString[9] ;

char* playergGetSecondsString(uint32_t seconds)
{
	char bufH[3],bufM[3],bufS[3];
	uint8_t hour,minute,second;

	hour =   seconds/3600;
	minute = (seconds%3600)/60;
	second = seconds%60;

	long_to_ascii(hour,   bufH, decimal);
	long_to_ascii(minute, bufM, decimal);
	long_to_ascii(second, bufS, decimal);

	if(hour/10==0)
	{
		strcpy(secondsString,"0");
		strcat(secondsString,bufH);
	}
	else
	{
		strcpy(secondsString,bufH);
	}
	strcat(secondsString,":");
	if(minute/10==0)	strcat(secondsString,"0");;
	strcat(secondsString,bufM);
	strcat(secondsString,":");
	if(second/10==0)	strcat(secondsString,"0");;
	strcat(secondsString,bufS);

	return secondsString;
}


void playerSecondsDisplay (void)
{
	const uint8_t yPosition = 54;

	sh1106_draw_bufor_rect(0,yPosition,56,10,black);
	sh1106_draw_bufor_rect(71,yPosition,56,10,black);

	sh1106_draw_bufor_string(0,yPosition,playergGetSecondsString(description.secondsPlayed),&Font_7x10,white,transparent);
	sh1106_draw_bufor_string(71,yPosition,playergGetSecondsString(description.secondsAmount),&Font_7x10,white,transparent);
}


void playerStatusBarDisplay (void)
{
	const uint8_t heigh = 6;
	const uint8_t yPosition = 45;
	uint8_t width;

	width = ( (description.secondsPlayed*126) / (description.secondsAmount+1) );

	sh1106_draw_bufor_rect(0,yPosition,128,heigh,white);
	sh1106_draw_bufor_rect(width+1,yPosition+1,127-(width+1),heigh-2,black);
}


void playerPlayPauseDisplay (void)
{
	sh1106_draw_bufor_rect(56,51,15,13,black);

	if(status.playPause==play)
	{
		sh1106_draw_bufor_line(58,52,69,58,white);
		sh1106_draw_bufor_line(58,63,69,58,white);
		sh1106_draw_bufor_line(58,52,58,63,white);
	}
	else
	{
		sh1106_draw_bufor_rect(60,53,3,10,white);
		sh1106_draw_bufor_rect(64,53,3,10,white);
	}
	sh1106_refresh_display();
}


void playerSongStatusDisplay (uint8_t flag)
{
	if(flag)
	{
		playerStatusBarDisplay();
		playerSecondsDisplay();
		sh1106_refresh_display();
	}
}

void playerDisplay(void)
{
	sh1106_set_bufor(black);
	sh1106_draw_bufor_string(0,0,playerGetSongName(pathString),&Font_7x10,white,transparent);

	playerSongStatusDisplay(1);

	sh1106_refresh_display();
}




/**************************************** player ****************************************/


void playerInit (FIL *file,UINT *br, char *path)
{
	f_open(file,path,FA_READ);
	playerGetWavDescription(file);
	soundMode = playerGetSoundMode();
	playerSampleSpeedCalculation();
	playerGetAmountOfSeconds();
	playerGetScrollSPeed();
	f_read(file,globalFileBuffer, GLOBAL_BUFFER_SIZE, br);
}


void playerPlaySong (void)
{
	static FIL file;
	UINT br;

	if(status.continueFlag==reset)
	{
		status.continueFlag=set;
		playerInit(&file,&br,pathString);
		TaskManager_Edit_TaskStatus(ID_WAV_PLAYER,AWAIT);
		playerPlayPauseControl(1);
		status.playPause = play;
		byteRead = 0;
		playerDisplay();
		playerPlayPauseDisplay();
	}

	if(status.continueFlag==set)
	{
		if(status.scrollingFlag==reset)
		{
			if(status.switcherFlag==reset && offsetData>((GLOBAL_BUFFER_SIZE/2)-1))
			{
				status.switcherFlag = set;
				f_read(&file,globalFileBuffer, GLOBAL_BUFFER_SIZE/2, &br);
				byteRead += br;
				if (br < (GLOBAL_BUFFER_SIZE/2))	status.continueFlag=reset;
			}

			if(status.switcherFlag==set && offsetData<(GLOBAL_BUFFER_SIZE/2))
			{
				status.switcherFlag = reset;
				f_read(&file,&globalFileBuffer[GLOBAL_BUFFER_SIZE/2], GLOBAL_BUFFER_SIZE/2, &br);
				byteRead += br;
				if (br < (GLOBAL_BUFFER_SIZE/2))	status.continueFlag=reset;
			}

			playerSongStatusDisplay( playerGetPlayedSeconds() );
		}
		else if(status.scrollingFlag==lseekUpdate)
		{
			status.scrollingFlag = reset;
			f_lseek(&file,byteRead);
			f_read(&file,globalFileBuffer, GLOBAL_BUFFER_SIZE, &br);
			playerPlayPauseControl(1);
		}
		else if(status.scrollingFlag==set)
		{
			playerPlayPauseControl(0);
		}
	}

	if(status.continueFlag==reset)
	{
		status.playPause = pause;
		byteRead = 0;
	}
}


/**************************************** handler ****************************************/


void navigationOnWavPlayer (void)
{
	if(qwerty_GetKeyStatusAndClear(5)==pressed)		// play pause
	{
		if(status.playPause==pause)
		{
			status.playPause = play;
			TaskManager_Edit_TaskStatus(ID_WAV_PLAYER,AWAIT);
			playerPlayPauseControl(1);
		}
		else
		{
			status.playPause = pause;
			TaskManager_Edit_TaskStatus(ID_WAV_PLAYER,BLOCKED);
			playerPlayPauseControl(0);
		}
		playerPlayPauseDisplay();
	}

	if(status.scrollingFlag==set)	status.scrollingFlag = lseekUpdate;

	static uint16_t cnt1=1000;
	if(qwerty_GetKeyStatusAndClear(4)==held_still)	// left
	{
		if(byteRead > scrollSpeed*(cnt1/1000) && byteRead-scrollSpeed*(cnt1/1000)>0)
		{
			byteRead -= scrollSpeed*(cnt1/1000);
			if(cnt1<=4000)
			{
				cnt1 = cnt1 * 1001 / 1000;
			}
			status.scrollingFlag = set;
		}
	}
	else	cnt1=1000;

	static uint16_t cnt2=1000;
	if(qwerty_GetKeyStatusAndClear(6)==held_still)	// right
	{
		if(byteRead+scrollSpeed*(cnt2/1000)<description.dataSize)
		{
			byteRead += scrollSpeed*(cnt2/1000);
			if(cnt2<=4000)
			{
				cnt2 = cnt2 * 1001 / 1000;
			}
			status.scrollingFlag = set;
		}
	}
	else	cnt2=1000;

	if(status.scrollingFlag==set)
	{
		static uint16_t cnt=0;

		playerPlayPauseControl(0);

		cnt++;
		if(cnt>=20)
		{
			cnt=0;
			playerSongStatusDisplay( playerGetPlayedSeconds() );
		}
	}

	if(qwerty_GetKeyStatusAndClear(12)==pressed)	// exit
	{
		status.continueFlag = reset;

		TaskManager_Edit_TaskStatus(ID_WAV_PLAYER,BLOCKED);
		playerPlayPauseControl(0);
		actualMode = TYPE_DIR;
		cutLastStringFromPath(pathString);
		sh1106_set_bufor(black);
		catalogRedraw();
		byteRead=0;
	}

	uint8_t flagresol=reset;
	if(qwerty_GetKeyStatusAndClear(3)==pressed)
	{
		if(resol<15)
		{
			resol++;
			flagresol=set;
		}
	}

	if(qwerty_GetKeyStatusAndClear(7)==pressed)
	{
		if(resol>0)
		{
			resol--;
			flagresol=set;
		}
	}

	if(flagresol==set)
	{
		flagresol=reset;
		sh1106_draw_bufor_rect(50,30,20,10,black);
		sh1106_draw_bufor_integer(50,30,16-resol,10,&Font_7x10,white,transparent);
		sh1106_refresh_display();
	}
}


/**************************************** sample interrupt ****************************************/


void TIM2_IRQHandler (void)
{
	if(SAMPLE_TIMER->SR & TIM_SR_UIF)
	{
		SAMPLE_TIMER->SR &= ~TIM_SR_UIF;

		register int16_t  buf1,buf2;

		switch(soundMode)
		{
			case res16ch2:
			buf1  = globalFileBuffer[offsetData++];
			buf1 += (globalFileBuffer[offsetData++]<<8);
			buf1 >>= resol;

			buf2  = globalFileBuffer[offsetData++];
			buf2 += (globalFileBuffer[offsetData++]<<8);
			buf2 >>= resol;

			PWM_CH1 =  (buf1+32768) >> 8;
			PWM_CH2 = ((buf1+32768)) & 0x00FF;
			PWM_CH3 =  (buf2+32768) >> 8;
			PWM_CH4 = ((buf2+32768)) & 0x00FF;
			break;

			case res16ch1:
			buf1 = globalFileBuffer[offsetData++];
			buf2 = globalFileBuffer[offsetData++];
			PWM_CH2 = buf1;
			PWM_CH4 = buf1;
			PWM_CH1 = buf2;
			PWM_CH3 = buf2;
			break;

			case res8ch2:
			buf1 = globalFileBuffer[offsetData++];
			buf2 = globalFileBuffer[offsetData++];
			PWM_CH1 = buf1;
			PWM_CH2 = buf1;
			PWM_CH3 = buf2;
			PWM_CH4 = buf2;
			break;

			case res8ch1:
			buf1 = globalFileBuffer[offsetData++];
			PWM_CH1 = buf1;
			PWM_CH2 = buf1;
			PWM_CH3 = buf1;
			PWM_CH4 = buf1;
			break;

			default:
			break;
		}
		if (offsetData >= GLOBAL_BUFFER_SIZE)	offsetData = 0;
	}
}
