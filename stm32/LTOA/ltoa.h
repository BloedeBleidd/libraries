/*
 * ltoa.h
 *
 *  Created on: Nov 12, 2016
 *      Author: BloedeBleidd
 */

#ifndef LTOA_LTOA_H_
#define LTOA_LTOA_H_

#include "stm32f10x.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


typedef enum
{
	binary = 2,
	decimal = 10,
	hexadecimal = 16,
}base_t;

char* long_to_ascii(int32_t value, char* buffer, base_t base);

#endif /* LTOA_LTOA_H_ */
