/*
 * fifo.c
 *
 *  Created on: Nov 15, 2016
 *      Author: BloedeBleidd
 */

#include "fifo.h"


bool fifo_Add(fifoBuffer_t *cb, fifoDataElement elem)
{
//	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
//	{
	if(fifo_IsFull(cb)) return false;
	uint8_t end = (cb->Beg + cb->Count) % CB_MAXTRANS;
	cb->element[end] = elem;
	++cb->Count;
//	}
	return true;
}

fifoDataElement fifo_Read(fifoBuffer_t *cb)
{
	fifoDataElement elem;
//	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
//	{
	if(fifo_IsEmpty(cb)) return 0;
	elem = cb->element[cb->Beg];
	cb->Beg = (cb->Beg + 1) % CB_MAXTRANS;
	-- cb->Count;
//	}
	return elem;
}
