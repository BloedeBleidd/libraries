/*
 * fifo.h
 *
 *  Created on: Nov 15, 2016
 *      Author: BloedeBleidd
 */

#ifndef FIFO_FIFO_H_
#define FIFO_FIFO_H_

#include "stm32f10x.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#define CB_MAXTRANS 40

typedef uint8_t fifoDataElement;

typedef struct
{
	fifoDataElement element[CB_MAXTRANS];
	uint16_t Beg;
	uint16_t Count;
}fifoBuffer_t;

bool fifo_Add(fifoBuffer_t *cb, fifoDataElement elem);
fifoDataElement fifo_Read(fifoBuffer_t *cb);

static inline bool fifo_IsFull(fifoBuffer_t *cb)
{
	return cb->Count == CB_MAXTRANS;
}

static inline bool fifo_IsEmpty(fifoBuffer_t *cb)
{
	return cb->Count == 0;
}

#endif /* FIFO_FIFO_H_ */
