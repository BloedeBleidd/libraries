/*
 * taskmanager.h
 *
 *  Created on: Nov 3, 2016
 *      Author: BloedeBleidd
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifndef TASKMANAGER_H_
#define TASKMANAGER_H_

#include "TaskManager/taskmanagerIDs.h"
#include "LTOA/ltoa.h"


/**************************************** Configuration ****************************************/

#define MAX_TASKS			10
#define NUMBER_OF_TIMERS 	4

#define USE_PROGRAM_TIMERS	1
#define USE_CPU_USAGE 		1
#define USE_SYS_TICK_CNT 	0

/**************************************** Timers ****************************************/

#define CPU_USAGE_TIMER		TIM4
#define CPU_TIMER_ENABLE	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
#define CPU_TIMER_START		CPU_USAGE_TIMER->CR1 |= TIM_CR1_CEN;
#define CPU_TIMER_STOP		CPU_USAGE_TIMER->CR1 &= ~TIM_CR1_CEN;
#define CPU_USAGE_COUNTER	CPU_USAGE_TIMER->CNT

#if USE_PROGRAM_TIMERS == 1
#define NUMBER_OF_PROGRAM_TIMERS NUMBER_OF_TIMERS
extern volatile uint16_t programTimer[NUMBER_OF_PROGRAM_TIMERS];
#endif


#if USE_SYS_TICK_CNT  == 1
extern volatile uint32_t systemTickCounter;
#endif

/**************************************** Scheduler ****************************************/

#define ERROR		0
#define OK			1
#define BLOCKED		2
#define AWAIT		3
#define TO_HANDLE	4

typedef void (*task_t)(void);

typedef struct
{
	uint8_t priority;			// priority of task
	volatile uint8_t status; 	// status of task
	volatile uint16_t delay;	// delay time
	uint16_t period;			// period time
	task_t task;				// pointer to the task
} tcb_t;



// scheduler functions
void TaskManagerInitialize(uint32_t);
uint8_t TaskManagerReplaceAddTask(uint8_t, uint8_t, task_t, uint16_t);
uint8_t TaskManagerEditTaskStatus(uint8_t, uint8_t);
uint8_t TaskManagerGetTaskStatus(uint8_t);
uint8_t TaskManagerEditTaskPeriod(uint8_t, uint16_t);
void TaskManagerHandlingTasks(void);

char* TaskManagerGetCpuUsageString(void);
uint16_t TaskManagerGetCpuUsage(void);


#endif /* TASKMANAGER_H_ */

#ifdef __cplusplus
}
#endif
