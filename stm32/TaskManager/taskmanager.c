/*
 * taskmanager.c
 *
 *  Created on: Nov 3, 2016
 *      Author: BloedeBleidd
 */

#include "taskmanager.h"

void SystemTickInitialize (uint32_t samples);

/**************************************** Scheduler ****************************************/

typedef void (*task_t)(void);

tcb_t task_list[MAX_TASKS];


#if USE_PROGRAM_TIMERS == 1
volatile uint16_t programTimer[NUMBER_OF_PROGRAM_TIMERS];
#endif

#if USE_SYS_TICK_CNT  == 1
volatile uint32_t systemTickCounter;
#endif


#if USE_CPU_USAGE  == 1

volatile uint16_t cpuUsage;
char cpuUsageString[6] ;

char* TaskManagerGetCpuUsageString(void)
{
	const uint16_t full = 9999;
	char buf1[3],buf2[2];

	if(cpuUsage > full)
	{
		cpuUsage = 9999;
	}
	long_to_ascii(cpuUsage/100, buf1, decimal);
	long_to_ascii(cpuUsage%100/10, buf2, decimal);
	strcpy(cpuUsageString,buf1);
	strcat(cpuUsageString,".");
	strcat(cpuUsageString,buf2);
	strcat(cpuUsageString,"%");

	return cpuUsageString;
}

uint16_t TaskManagerGetCpuUsage(void)
{
	return cpuUsage;
}

#endif

void TaskManagerInitialize(uint32_t samples)
{
	SystemTickInitialize(samples);

	#if USE_CPU_USAGE  == 1
	CPU_TIMER_ENABLE;
	CPU_USAGE_TIMER->PSC = 7200-1;
	CPU_USAGE_TIMER->ARR = 65000-1;
	#endif

	for(uint8_t i=0; i<MAX_TASKS; i++)
	{
		task_list[i].task = (task_t)0x00;
		task_list[i].delay = 0;
		task_list[i].period = 0;
		task_list[i].status = BLOCKED;
	}
}

uint8_t TaskManagerReplaceAddTask(uint8_t id, uint8_t priority, task_t task, uint16_t period)
{
	if(id >= MAX_TASKS-1)	return ERROR;
	task_list[id].priority = priority;
	task_list[id].status = AWAIT;
	task_list[id].delay = period;
	task_list[id].period = period;
	task_list[id].task = task;
	return OK;
}

uint8_t TaskManagerEditTaskStatus(uint8_t id, uint8_t status)
{
	if(id >= MAX_TASKS-1)	return ERROR;
	task_list[id].status = status;
	return OK;
}

uint8_t TaskManagerGetTaskStatus(uint8_t id)
{
	if(id >= MAX_TASKS-1)	return ERROR;
	else					return task_list[id].status;
}

uint8_t TaskManagerEditTaskPeriod(uint8_t id, uint16_t period)
{
	if(id >= MAX_TASKS-1)	return ERROR;
	task_list[id].period = period;
	return OK;
}


void TaskManagerHandlingTasks(void)
{
	uint8_t id = 0;
	uint8_t highest_priority = 255;

	for(uint8_t i=0;i<MAX_TASKS;i++)
	{
		if( task_list[i].status == TO_HANDLE)
		{
			if(task_list[i].priority < highest_priority)
			{
				highest_priority = task_list[i].priority;
				id = i;
			}
		}
	}

	if(highest_priority < 255)
	{
		task_list[id].delay = task_list[id].period;	// reset the delay
		if(!(task_list[id].period))	task_list[id].status = TO_HANDLE;
		else						task_list[id].status = AWAIT;

		#if USE_CPU_USAGE  == 1
		CPU_TIMER_START;
		#endif

		(*task_list[id].task)();					// call the task

		#if USE_CPU_USAGE  == 1
		CPU_TIMER_STOP;
		#endif
	}

}


/**************************************** Timers ****************************************/

void SystemTickInitialize (uint32_t samples)	//ctc mode,prescaller,cnt value
{
	if (SysTick_Config(SystemCoreClock / samples))
		while (1);
}

void SysTick_Handler(void)
{



/**************************************** TaskManager handling ****************************************/
	for(uint8_t i=0;i<MAX_TASKS;i++)
	{
		if( task_list[i].status == AWAIT )
		{
			if(task_list[i].delay)	task_list[i].delay--;
			else					task_list[i].status = TO_HANDLE;
		}
	}

	#if USE_SYS_TICK_CNT  == 1
	systemTickCounter++;
	#endif

	#if USE_PROGRAM_TIMERS == 1
	for(uint8_t i=0;i<NUMBER_OF_PROGRAM_TIMERS;i++)	if(programTimer[i])	programTimer[i]--;
	#endif

	#if USE_CPU_USAGE  == 1
	static uint16_t cycle=0;
	if(cycle++>=1000)
	{
		cycle = 0;
		cpuUsage = CPU_USAGE_COUNTER;
		CPU_USAGE_COUNTER = 0;
	}
	#endif
/**************************************** TaskManager handling ****************************************/
}
