#include "serialDataDMA.h"


void serialDataDMA::dmaReceiveInitialize(uint32_t dataRegister)
{
	receiveDmaChannel->CCR &= ~DMA_CCR1_EN;
	receiveDmaChannel->CPAR = (uint32_t)dataRegister;
	receiveDmaChannel->CMAR = (uint32_t)(&receiveData[0]);
	receiveDmaChannel->CNDTR = receiveBufferLength;
	receiveDmaChannel->CCR = DMA_CCR1_MINC | DMA_CCR1_CIRC;
	receiveDmaChannel->CCR |= DMA_CCR1_EN;
}


void serialDataDMA::dmaTransmitInitialize(uint32_t dataRegister)
{
	transmitDmaChannel->CCR &= ~DMA_CCR1_EN;
	transmitDmaChannel->CPAR = (uint32_t)dataRegister;
	transmitDmaChannel->CCR = DMA_CCR1_MINC | DMA_CCR1_DIR | DMA_CCR1_TCIE;
}


void serialDataDMA::dmaTransactionConfig(transmitData_t transmitData)
{
	transmitDmaChannel->CCR &= ~DMA_CCR1_EN;
	transmitDmaChannel->CMAR = (uint32_t)(&transmitData.str[0]);
	transmitDmaChannel->CNDTR = transmitData.length;
	transmitDmaChannel->CCR |= DMA_CCR1_EN;
}
