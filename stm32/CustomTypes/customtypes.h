/*
 * customtypes.h
 *
 *  Created on: 14.01.2017
 *      Author: BloedeBleidd
 */

#ifndef CUSTOMTYPES_CUSTOMTYPES_H_
#define CUSTOMTYPES_CUSTOMTYPES_H_


typedef int8_t I8;
typedef int16_t I16;
typedef int32_t I32;

typedef uint8_t U8;
typedef uint16_t U16;
typedef uint32_t U32;


typedef int16_t SHORT;
typedef int32_t LONG;

typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;


#endif /* CUSTOMTYPES_CUSTOMTYPES_H_ */
