
#include <stdint.h>

#include "../inc/stm32f4xx.h"

#include "../GPIO/gpio.h"

#include "d_led.h"


volatile uint8_t digit[4];
volatile uint8_t dot[4];


// definicja tablicy zawierającej definicje bitowe cyfr LED
const uint8_t cyfry[13] =
{
		(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F),			// 0
		(SEG_B|SEG_C),									// 1
		(SEG_A|SEG_B|SEG_D|SEG_E|SEG_G),				// 2
		(SEG_A|SEG_B|SEG_C|SEG_D|SEG_G),				// 3
		(SEG_B|SEG_C|SEG_F|SEG_G),						// 4
		(SEG_A|SEG_C|SEG_D|SEG_F|SEG_G),				// 5
		(SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G),			// 6
		(SEG_A|SEG_B|SEG_C|SEG_F),						// 7
		(SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G),	// 8
		(SEG_A|SEG_B|SEG_C|SEG_D|SEG_F|SEG_G),			// 9
		(0x00),										    // 10 NIC (puste miejsce)
		(SEG_G)									    // 11 kropka
};



void d_led_init(void)
{
	for(uint8_t i=0;i<8;i++)
	{
		gpio_pin_cfg(GPIO_LED, i, GPIO_OUT_PP_2MHz);
	}

	gpio_pin_cfg(GPIO_TRAN, 8, GPIO_OUT_PP_2MHz);
	gpio_pin_cfg(GPIO_TRAN, 9, GPIO_OUT_PP_2MHz);
	gpio_pin_cfg(GPIO_TRAN, 10, GPIO_OUT_PP_2MHz);
	gpio_pin_cfg(GPIO_TRAN, 11, GPIO_OUT_PP_2MHz);


	// ustawienie timera
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	TIM2->PSC = 999;
	TIM2->ARR = TIMER_ARR_VALUE;
	TIM2->DIER = TIM_DIER_UIE;
	TIM2->CR1 = TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM2_IRQn);
}


void d_led_display_int(uint16_t value)
{
	uint8_t buf[4];
	buf[0] = value % 10000 / 1000;
	buf[1] = value % 1000 / 100;
	buf[2] = value % 100 / 10;
	buf[3] = value % 10;

	for(uint8_t i=0;i<sizeof(buf);i++)	digit[i] = buf[i];
}


void TIM2_IRQHandler(void)
{
	if (TIM2->SR & TIM_SR_UIF)
	{
		TIM2->SR = ~TIM_SR_UIF;

		static uint8_t cnt=0;

		GPIO_TRAN->ODR &= ~(Pin8 | Pin9 | Pin10 | Pin11);

		if(dot[cnt] == 1) LED_DATA = cyfry[digit[cnt]] | SEG_DP;
		else LED_DATA = cyfry[digit[cnt]];

		switch(cnt)
		{
			case 0:
			GPIO_TRAN->BSRRL = Pin8;
			break;

			case 1:
			GPIO_TRAN->BSRRL = Pin9;
			break;

			case 2:
			GPIO_TRAN->BSRRL = Pin10;
			break;

			case 3:
			GPIO_TRAN->BSRRL = Pin11;
			break;
		}

		if(++cnt == 4)	cnt = 0;
	}
}

