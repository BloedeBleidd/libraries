
#include "../inc/stm32f4xx.h"

#include "../GPIO/gpio.h"

#ifndef _d_led_h
#define _d_led_h


#define	DISPLAY_REFRESH_RATE	120
#define	TIMER_ARR_VALUE 		(90000ul/4/DISPLAY_REFRESH_RATE-1)


#define	GPIO_LED	GPIOE
#define	GPIO_TRAN	GPIOE

#define LED_DATA 	GPIO_LED->ODR
#define TRAN_DATA 	GPIO_TRAN->ODR


#define SEG_A (1<<7)
#define SEG_B (1<<6)
#define SEG_C (1<<5)
#define SEG_D (1<<4)
#define SEG_E (1<<3)
#define SEG_F (1<<2)
#define SEG_G (1<<1)
#define SEG_DP (1<<0)

#define NIC 10
#define MINUS 11



extern volatile uint8_t digit[4];
extern volatile uint8_t dot[4];


void d_led_init(void);
void d_led_display_int(uint16_t);

#endif	// koniec _d_led_h

