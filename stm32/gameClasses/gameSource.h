/*
 * gameSource.h
 *
 *  Created on: 15.01.2017
 *      Author: BloedeBleidd
 */

#ifndef GAMECLASSES_GAMESOURCE_H_
#define GAMECLASSES_GAMESOURCE_H_

#include "stm32f10x.h"

const uint8_t OFFSET 		= 1;
const uint8_t HOLE			= 30;
const int16_t HIGHER_Y 		= -30;
const int16_t GRAVITY	 	= 210;
const int16_t VELOCITY_MAX 	= 2500;
const int16_t JUMP		 	= -3200;

class Bird;

class Drop
{
private:
	uint8_t x;
	int8_t y;
	uint8_t length;
	uint8_t speed;

	void randomizeDrop();

public:
    Drop();

    void fall();
    void show();
};


class Obstacle
{
private:
	int16_t x;
	int16_t xTmp;
	int16_t y1,y2,y3;
	int16_t width;
	int16_t speed;

public:
	Obstacle(uint16_t);

	bool isOutOffScreen();
	void move();
	void show();
	void setX(int16_t);

	friend bool collision(Bird &bird, Obstacle &obstacle);
};


class Bird
{
private:
	uint8_t x;
	int8_t y;
	uint8_t width;
	uint8_t height;
	int32_t speed;

public:
	Bird();

	void jump();
	void move();
	void show();

	friend bool collision(Bird &bird, Obstacle &obstacle);
};



#endif /* GAMECLASSES_GAMESOURCE_H_ */
