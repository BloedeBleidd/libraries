/*
 * gameSource.cpp
 *
 *  Created on: 15.01.2017
 *      Author: BloedeBleidd
 */

#include "gameSource.h"
#include <cstdlib>
#include <string>
#include <cstring>
#include "SH1106/sh1106.h"

Drop::Drop()
{
   	randomizeDrop();
}

void Drop::randomizeDrop()
{
	x		= rand() % 128;
	y		= rand() % 10 - 10;
	length	= rand() % 7 + 3;
	speed	= rand() % 6 + 4;
}

void Drop::fall()
{
	y += speed;
	if(y>=64)
	{
		randomizeDrop();
	}
}

void Drop::show()
{
	sh1106_draw_bufor_vline(x, y, y+length, white);
}



Obstacle::Obstacle(uint16_t velocity)
{
	x 		= 128;
	xTmp	= 12800;
	y1 		= 0;
	y2		= OFFSET + (rand() % (64-OFFSET*2-HOLE) );
	y3		= y2 + HOLE;
	width	= 10;
	speed 	= rand() % velocity + 50;
}


void Obstacle::move()
{
	xTmp =  xTmp - speed;
    x = xTmp / 100;
}

bool Obstacle::isOutOffScreen()
{
	if( x + width <= 0 )	return true;
	else	return false;
}

void Obstacle::show()
{
	sh1106_draw_bufor_rect(x, y1, width, y2-y1, white);
	sh1106_draw_bufor_rect(x, y3, width, 64-y3, white);
}

void Obstacle::setX(int16_t newX)
{
    x = newX;
}


Bird::Bird()
{
	x = 30;
	y = 10;
	width = 5;
	height = 5;
	speed = 0;
}

void Bird::move()
{
	speed += GRAVITY;
	if(speed > VELOCITY_MAX)
	{
		speed = VELOCITY_MAX;
	}
	y += speed/1000;
	if(y<HIGHER_Y)	y = HIGHER_Y;
}

void Bird::jump()
{
	speed = JUMP;
}

void Bird::show()
{
	sh1106_draw_bufor_rect(x, y, width, height, white);
}


bool collision(Bird &bird, Obstacle &obstacle)
{
	if		( (bird.x+bird.width) >= (obstacle.x) && (bird.x+bird.width) <= (obstacle.x+obstacle.width) && (bird.y) <= (obstacle.y2))				return true;
	else if	( (bird.x+bird.width) >= (obstacle.x) && (bird.x+bird.width) <= (obstacle.x+obstacle.width) && (bird.y+bird.height) >= (obstacle.y3))	return true;
	else if	( (bird.y+bird.height) >= 63)																							return true;

	return false;
}




