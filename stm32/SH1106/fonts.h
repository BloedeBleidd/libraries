/*
 * fonts.h
 *
 *  Created on: 24.01.2017
 *      Author: BloedeBleidd
 */

#ifndef SH1106_FONTS_H_
#define SH1106_FONTS_H_

typedef struct
{
	uint8_t FontWidth;    /*!< Font width in pixels */
	uint8_t FontHeight;   /*!< Font height in pixels */
	const uint16_t *data; /*!< Pointer to data font data array */
} Font;

extern Font Font_7x10;
extern Font Font_11x18;
extern Font Font_16x26;



#endif /* SH1106_FONTS_H_ */
