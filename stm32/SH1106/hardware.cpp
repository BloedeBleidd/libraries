/*
 * hardware.cpp
 *
 *  Created on: 24.01.2017
 *      Author: BloedeBleidd
 */

#include "sh1106.h"


SH1106::SH1106( SPI_TypeDef *_spi, GPIO_TypeDef *_gpio, PinNumber _mosi, PinNumber _clock, PinNumber _chipSelect, PinNumber _dataCommand, RefreshRate refresh, uint8_t contrast )
{
	spi			= _spi;
	gpio		= _gpio;
	mosi		= _mosi;
	clock		= _clock;
	chipSelect	= _chipSelect;
	dataCommand = _dataCommand;

	xCursor = 0;
	yCursor = 0;
	buffer = new uint8_t [bufferLength];

	SH1106::HardwareInitialize();
	SH1106::sh1106Initialize( refresh, contrast );
}

void SH1106::csControl( CsControl state )
{
	if( state == select )
	{
		gpio->ODR &= ~chipSelect;
	}
	else
	{
		SH1106::waitUntilSpiIsBusy();
		gpio->ODR |= chipSelect;
	}
}

void SH1106::dcControl(DcControl state)
{
	SH1106::waitUntilSpiIsBusy();

	if(state==command)
	{
		gpio->ODR &= ~dataCommand;
	}
	else
	{
		gpio->ODR |= dataCommand;
	}
}

void SH1106::waitUntilSpiTransmits()
{
	while(!(spi->SR & SPI_SR_TXE))
	{

	}
}

void SH1106::waitUntilSpiIsBusy()
{
	while(spi->SR & SPI_SR_BSY)
	{

	}
}

void SH1106::HardwareInitialize()
{
	// SCK, MOSI, DC, CS
	gpioPinConfiguration(gpio, mosi,		gpio_mode_alternate_PP_50MHz);
	gpioPinConfiguration(gpio, clock,		gpio_mode_alternate_PP_50MHz);
	gpioPinConfiguration(gpio, chipSelect,	gpio_mode_output_PP_50MHz);
	gpioPinConfiguration(gpio, dataCommand,	gpio_mode_output_PP_50MHz);
	csControl(deselect);

	// init spi
	spi->CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_CPOL | SPI_CR1_CPHA;
	spi->CR1 |= SPI_CR1_SPE;
}

void SH1106::sendConfigData( ConfigRegister data )
{
	SH1106::waitUntilSpiTransmits();
	spi->DR = (uint8_t)data;
}

void SH1106::sendData( uint8_t data )
{
	SH1106::waitUntilSpiTransmits();
	spi->DR = data;
}

void SH1106::sh1106Initialize( RefreshRate refresh, uint8_t contrast )
{
	csControl(select);
	for( volatile uint32_t i=0; i<1000000; i++ ){};	//delay 10ms
	csControl(deselect);

	csControl(select);
	dcControl(command);

	SH1106::sendConfigData( DISPLAYOFF );
	SH1106::sendConfigData( SETMULTIPLEX );
	SH1106::sendData( 0x3F );
	SH1106::sendConfigData( SETSTARTLINE );			/*set display start line*/
	SH1106::sendConfigData( PAGEADDR );   		 	/*set page address*/
	SH1106::sendData( SETLOWCOLUMN | 0x02 );		/*set lower column address*/
	SH1106::sendConfigData( SETHIGHCOLUMN );    	/*set higher column address*/
	SH1106::sendData( SEGREMAP | 0x01 ); 			/*set segment remap*/
	SH1106::sendConfigData( NORMALDISPLAY );    	/*normal / reverse*/
	SH1106::sendConfigData( CHARGEPUMP );			/*set charge pump enable*/
	SH1106::sendConfigData( MEMORYMODE );
	SH1106::sendData(0x00 );
	SH1106::sendConfigData( EXTERNALVCC );			/*external VCC   */
	SH1106::sendData( 0x31 );  						/*0X30---0X33  7,4V-8V-8,4V-9V */
	SH1106::sendConfigData( COMSCANDEC );    		/*Com scan direction*/
	SH1106::sendConfigData( SETDISPLAYOFFSET );    	/*set display offset*/
	SH1106::sendData( 0x00 );   					/*   0x20  /   0x00 */
	SH1106::sendConfigData( SETDISPLAYCLOCKDIV );  	/*set osc division*/
	SH1106::sendData( (uint8_t)refresh );
	SH1106::sendConfigData( SETPRECHARGE );    		/*set pre-charge period*/
	SH1106::sendData( 0x1f );    					/*0x22  /  0x1f*/
	SH1106::sendConfigData( SETCOMPINS );    		/*set COM pins*/
	SH1106::sendData( 0x12 );
	SH1106::sendConfigData( SETVCOMDETECT );    	/*set vcomh*/
	SH1106::sendData( 0x40 );
	SH1106::sendConfigData( SETCONTRAST );
	SH1106::sendData( contrast );

	SH1106::sendConfigData( DISPLAYALLON_RESUME );
	SH1106::sendConfigData( NORMALDISPLAY );
	// some free time
	SH1106::fill( black );
	// turn on oled panel
	SH1106::sendConfigData( DISPLAYON );

	csControl( deselect );

	SH1106::refresh();
}

void SH1106::refresh()
{
	uint16_t cnt = 0;
	csControl( select );
	dcControl( command );
	SH1106::sendConfigData( SETLOWCOLUMN );

	for( uint8_t i=0; i<8; i++ )
	{
		dcControl( command );
		SH1106::sendData( 0xB0 + i );
		SH1106::sendConfigData( SETHIGHCOLUMN );
		dcControl( data );

	    for( uint8_t j=0; j<128; j++ )
	    {
	    	SH1106::sendData( buffer[cnt] );
	    	cnt++;
	    }
	}
}


