/*
 * sh1106.h
 *
 *  Created on: 24.01.2017
 *      Author: BloedeBleidd
 */

#ifndef SH1106_SH1106_H_
#define SH1106_SH1106_H_


#include "stm32f10x.h"
#include "fonts.h"
#include "GPIO/gpio.h"

enum ConfigRegister
{
	SETCONTRAST			= 0x81,
	DISPLAYALLON_RESUME = 0xA4,
	DISPLAYALLON		= 0xA5,
	NORMALDISPLAY		= 0xA6,
	INVERTDISPLAY		= 0xA7,
	DISPLAYOFF			= 0xAE,
	DISPLAYON			= 0xAF,
	SETDISPLAYOFFSET	= 0xD3,
	SETCOMPINS			= 0xDA,
	SETVCOMDETECT		= 0xDB,
	SETDISPLAYCLOCKDIV	= 0xD5,
	SETPRECHARGE		= 0xD9,
	SETMULTIPLEX		= 0xA8,
	SETLOWCOLUMN		= 0x02,    //ssd1306 has 0x00 SETLOWCOLUMN   in sh1106 we must sweep by 0x02 then SETLOWCOLUMN = 0x02
	SETHIGHCOLUMN		= 0x10,
	SETSTARTLINE		= 0x40,
	MEMORYMODE			= 0x20,
	COLUMNADDR			= 0x21,
	PAGEADDR			= 0xB0,    //ssd1306 has 0x22
	COMSCANINC			= 0xC0,
	COMSCANDEC			= 0xC8,
	SEGREMAP			= 0xA0,
	CHARGEPUMP			= 0xAD,    // ssd1306 has 0x8D
	EXTERNALVCC			= 0x8B,    // ssd1306 has 0x01
	SWITCHCAPVCC		= 0x02,
	// Scrolling
	ACTIVATE_SCROLL							= 0x2F,
	DEACTIVATE_SCROLL						= 0x2E,
	SET_VERTICAL_SCROLL_AREA				= 0xA3,
	RIGHT_HORIZONTAL_SCROLL					= 0x26,
	LEFT_HORIZONTAL_SCROLL					= 0x27,
	VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL	= 0x29,
	VERTICAL_AND_LEFT_HORIZONTAL_SCROLL		= 0x2A,
};

enum CsControl
{
	select		= 0,
	deselect	= 1,
};

enum DcControl
{
	data		= 0,
	command		= 1,
};

enum RefreshRate
{
	REFRESH_MIN = 0x80,
	REFRESH_MID = 0xB0,
	REFRESH_MAX = 0xF0,
};

enum Color
{
	black		= 0,
	white		= 1
};

enum Background
{
	transparent	= 0,
	filled		= 1
};

enum Base
{
	binary		= 2,
	decimal		= 10,
	hexadecimal	= 16,
};

const uint8_t displayWidth	= 128;
const uint8_t displayHeight = 64;
const uint16_t bufferLength = (displayWidth * displayHeight / 8);

class SH1106
{
public:
	int16_t xCursor, yCursor;

	SH1106( SPI_TypeDef *_spi, GPIO_TypeDef *_gpio, PinNumber _mosi, PinNumber _clock, PinNumber _chipSelect, PinNumber _dataCommand, RefreshRate refresh, uint8_t contrast );
	~SH1106();

	void refresh();
	void fill				( Color color );
	void drawPixel			( int16_t x, int16_t y, Color color );
	void drawVerticalLine	( int16_t x, int16_t yBeg, int16_t yEnd, Color color );
	void drawHorizontalLine	( int16_t xBeg, int16_t xEnd, int16_t y, Color color );
	void drawLine			( int16_t xBeg, int16_t yBeg, int16_t xEnd, int16_t yEnd, Color color );
	void drawRect			( int16_t x, int16_t y, int16_t width, int16_t height, Color color );
	void drawChar			( int16_t x, int16_t y, const char ch, Font* font, Color color, Background background );
	void drawString			( int16_t x, int16_t y, const char *str, Font *font, Color color, Background background );
	void drawInteger		( int16_t x, int16_t y, int32_t data, Base base, Font *font, Color color, Background background );

private:
	SPI_TypeDef		*spi;
	GPIO_TypeDef	*gpio;
	PinNumber		mosi;
	PinNumber		clock;
	PinNumber		chipSelect;
	PinNumber		dataCommand;
	uint8_t 		*buffer;

	void swap					( int16_t a, int16_t b );
	void csControl				( CsControl state );
	void dcControl				(DcControl state );
	void waitUntilSpiTransmits	();
	void waitUntilSpiIsBusy		();
	void HardwareInitialize		();
	void sendData				( uint8_t data );
	void sendConfigData			( ConfigRegister data );
	void sh1106Initialize		( RefreshRate refresh, uint8_t contrast );
};


/*
 * 	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	AFIO->MAPR |= AFIO_MAPR_SWJ_CFG_1 ; //release PB3
	AFIO->MAPR |= AFIO_MAPR_SPI1_REMAP;
 */

#endif /* SH1106_SH1106_H_ */
