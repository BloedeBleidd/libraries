/*
 * graphic.cpp
 *
 *  Created on: 24.01.2017
 *      Author: BloedeBleidd
 */

#include "sh1106.h"
#include <cstdlib>


void SH1106::swap( int16_t a, int16_t b )
{
	int16_t tmp;
	tmp = a;
	a = b;
	b = tmp;
}

void SH1106::fill( Color color )
{
	for(uint16_t i=0; i<bufferLength; i++)
	{
		buffer[i] = color;
	}
}

void SH1106::drawPixel( int16_t x, int16_t y, Color color )
{
	if( (x < 0) || (x >= displayWidth) || (y < 0) || (y >= displayHeight) )
	{
		return;
	}

	if( color==white )
	{
		buffer[x + (y / 8) * displayWidth]	|= (1<<(y%8));	// on pixel
	}
	else
	{
		buffer[x + (y / 8) * displayWidth]	&= ~(1<<(y%8));	// off pixel
	}
}

void SH1106::drawVerticalLine( int16_t x, int16_t yBeg, int16_t yEnd, Color color )
{
	if( yBeg < yEnd )
	{
		for( int16_t i=yBeg; i<yEnd; i++ )
		{
			SH1106::drawPixel( x, i, color );
		}
	}
	else
	{
		for( int16_t i=yBeg; i>yEnd; i-- )
		{
			SH1106::drawPixel( x, i, color );
		}
	}
}

void SH1106::drawHorizontalLine( int16_t xBeg, int16_t xEnd, int16_t y, Color color )
{
	if( xBeg < xEnd )
	{
		for( int8_t i=xBeg; i<xEnd; i++ )
		{
			SH1106::drawPixel( i, y, color );
		}
	}
	else
	{
		for( int8_t i=xBeg; i>xEnd; i-- )
		{
			SH1106::drawPixel( i, y, color );
		}
	}
}

void SH1106::drawLine( int16_t xBeg, int16_t yBeg, int16_t xEnd, int16_t yEnd, Color color )
{
	int16_t dx, dy;
	int16_t steep;
	int16_t ystep;
	int16_t err;

	steep = abs( yEnd - yBeg ) > abs( xEnd - xBeg );

	if( steep )
	{
		SH1106::swap( xBeg, yBeg );
		SH1106::swap( xEnd, yEnd );
	}

	if( xBeg > xEnd )
	{
		SH1106::swap( xBeg, xEnd );
		SH1106::swap( yBeg, yEnd );
	}

	dx = xEnd - xBeg;
	dy = abs( yEnd - yBeg );
	err = dx / 2;

	if( yBeg < yEnd )
	{
		ystep = 1;
	}
	else
	{
		ystep = -1;
	}

	for( ; xBeg <= xEnd; xBeg++ )
	{
		if( steep )
		{
			SH1106::drawPixel( yBeg, xBeg, color );
		}
		else
		{
			SH1106::drawPixel( xBeg, yBeg, color );
		}

		err -= dy;

		if( err < 0 )
		{
			yBeg += ystep;
			err += dx;
		}
	}
}

void SH1106::drawRect( int16_t x, int16_t y, int16_t width, int16_t height, Color color )
{
	for( int16_t i=x; i<x+width; i++ )
	{
		SH1106::drawVerticalLine( i, y, y+height, color );
	}
}


void SH1106::drawChar(int16_t x, int16_t y, const char ch, Font* font, Color color, Background background )
{
	uint32_t i, j, b;

	xCursor = x;
	yCursor = y;

	for ( i = 0; i<(font->FontHeight); i++ )
	{
		b = font->data[(ch - 32) * (font->FontHeight) + i];

		for ( j = 0; j<(font->FontWidth); j++ )
		{
			if ( (b << j) & 0x8000 )
			{
				SH1106::drawPixel( xCursor + j, (yCursor + i), color );
			}
			else if( background == filled )
			{
				SH1106::drawPixel( xCursor + j, (yCursor + i), black );
			}
		}
	}

	/* Increase pointer */
	xCursor += font->FontWidth;
}

void SH1106::drawString(int16_t x, int16_t y, const char *str, Font *font, Color color, Background background )
{
	xCursor = x;
	yCursor = y;

	/* Write characters */
	while (*str)
	{
		/* Write character by character */
		SH1106::drawChar( xCursor, yCursor, *str, font, color, background );

		/* Increase string pointer */
		str++;
	}
}

void SH1106::drawInteger(int16_t x, int16_t y, int32_t data, Base base, Font *font, Color color, Background background )
{
	char buf [12];

	itoa(data, buf, base);
	SH1106::drawString( x, y, buf, font, color, background );
}
