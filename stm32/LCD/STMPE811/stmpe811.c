/*
 * stmpe811.h
 *
 *  Created on: Sep 2, 2016
 *      Author: BloedeBleidd
 */

#include "stmpe811.h"


/****************************** Private defines ******************************/
// I2C address
#define STMPE811_ADDRESS				0x82

// STMPE811 Chip ID on reset
#define STMPE811_CHIP_ID_VALUE			0x0811	//Chip ID

// Registers
#define STMPE811_CHIP_ID				0x00	//STMPE811 Device identification
#define STMPE811_ID_VER					0x02	//STMPE811 Revision number; 0x01 for engineering sample; 0x03 for final silicon
#define STMPE811_SYS_CTRL1				0x03	//Reset control
#define STMPE811_SYS_CTRL2				0x04	//Clock control
#define STMPE811_SPI_CFG				0x08	//SPI interface configuration
#define STMPE811_INT_CTRL				0x09	//Interrupt control register
#define STMPE811_INT_EN					0x0A	//Interrupt enable register
#define STMPE811_INT_STA				0x0B	//Interrupt status register
#define STMPE811_GPIO_EN				0x0C	//GPIO interrupt enable register
#define STMPE811_GPIO_INT_STA			0x0D	//GPIO interrupt status register
#define STMPE811_ADC_INT_EN				0x0E	//ADC interrupt enable register
#define STMPE811_ADC_INT_STA			0x0F	//ADC interface status register
#define STMPE811_GPIO_SET_PIN			0x10	//GPIO set pin register
#define STMPE811_GPIO_CLR_PIN			0x11	//GPIO clear pin register
#define STMPE811_MP_STA					0x12	//GPIO monitor pin state register
#define STMPE811_GPIO_DIR				0x13	//GPIO direction register
#define STMPE811_GPIO_ED				0x14	//GPIO edge detect register
#define STMPE811_GPIO_RE				0x15	//GPIO rising edge register
#define STMPE811_GPIO_FE				0x16	//GPIO falling edge register
#define STMPE811_GPIO_AF				0x17	//alternate function register
#define STMPE811_ADC_CTRL1				0x20	//ADC control
#define STMPE811_ADC_CTRL2				0x21	//ADC control
#define STMPE811_ADC_CAPT				0x22	//To initiate ADC data acquisition
#define STMPE811_ADC_DATA_CHO			0x30	//ADC channel 0
#define STMPE811_ADC_DATA_CH1			0x32	//ADC channel 1
#define STMPE811_ADC_DATA_CH2			0x34	//ADC channel 2
#define STMPE811_ADC_DATA_CH3			0x36	//ADC channel 3
#define STMPE811_ADC_DATA_CH4			0x38	//ADC channel 4
#define STMPE811_ADC_DATA_CH5			0x3A	//ADC channel 5
#define STMPE811_ADC_DATA_CH6			0x3C	//ADC channel 6
#define STMPE811_ADC_DATA_CH7			0x3E	//ADC channel 7
#define STMPE811_TSC_CTRL				0x40	//4-wire touchscreen controller setup
#define STMPE811_TSC_CFG				0x41	//Touchscreen controller configuration
#define STMPE811_WDW_TR_X				0x42	//Window setup for top right X
#define STMPE811_WDW_TR_Y				0x44	//Window setup for top right Y
#define STMPE811_WDW_BL_X				0x46	//Window setup for bottom left X
#define STMPE811_WDW_BL_Y				0x48	//Window setup for bottom left Y
#define STMPE811_FIFO_TH				0x4A	//FIFO level to generate interrupt
#define STMPE811_FIFO_STA				0x4B	//Current status of FIFO
#define STMPE811_FIFO_SIZE				0x4C	//Current filled level of FIFO
#define STMPE811_TSC_DATA_X				0x4D	//Data port for touchscreen controller data access
#define STMPE811_TSC_DATA_Y				0x4F	//Data port for touchscreen controller data access
#define STMPE811_TSC_DATA_Z				0x51	//Data port for touchscreen controller data access
#define STMPE811_TSC_DATA_XYZ			0x52	//Data port for touchscreen controller data access
#define STMPE811_TSC_FRACTION_Z			0x56	//Touchscreen controller FRACTION_Z
#define STMPE811_TSC_DATA				0x57	//Data port for touchscreen controller data access
#define STMPE811_TSC_I_DRIVE			0x58	//Touchscreen controller drivel
#define STMPE811_TSC_SHIELD				0x59	//Touchscreen controller shield
#define STMPE811_TEMP_CTRL				0x60	//Temperature sensor setup
#define STMPE811_TEMP_DATA				0x61	//Temperature data access port
#define STMPE811_TEMP_TH				0x62	//Threshold for temperature controlled interrupt



volatile STMPE811_Data_t TouchPad;


/****************************** Private functions ******************************/
void STMPE811_ReadXY(void);


void STMPE811_Timer_Process (void)
{
	/* 1kHz decrement timer stopped at 0 */
	if (TouchPad.delay) TouchPad.delay--;
}

void Delayms (uint8_t delay)
{
	for(Program_Timer[0]=delay; Program_Timer[0];);
}

void STMPE811_WriteRegister (uint8_t addressRegister, uint8_t data)
{
	uint32_t dummy;

	I2C3->CR1 |= I2C_CR1_START;				// request a start
	while(!(I2C3->SR1 & I2C_SR1_SB ));		// wait for start to finish // read of SR1 clears the flag
	I2C3->DR = STMPE811_ADDRESS;			// transfer address (Transmit)
	while(!(I2C3->SR1 & I2C_SR1_ADDR));		// wait for address transfer
	dummy = I2C3->SR2;						// clear the flag

	while(!(I2C3->SR1 & I2C_SR1_TXE));		// wait for DR empty
	I2C3->DR = addressRegister;

	while(!(I2C3->SR1 & I2C_SR1_TXE));		// wait for DR empty
	I2C3->DR = data;						// transfer one byte

	while((!(I2C3->SR1 & I2C_SR1_TXE)) || (!(I2C3->SR1 & I2C_SR1_BTF)));// wait for bus not-busy
	I2C3->CR1 |= I2C_CR1_STOP;				// request a stop
}

void STMPE811_ReadRegisters(uint8_t addressRegister, uint8_t* data, uint8_t length)
{
	uint32_t dummy;

	I2C3->CR1 |= I2C_CR1_ACK;

	I2C3->CR1 |= I2C_CR1_START;				// request a start
	while(!(I2C3->SR1 & I2C_SR1_SB));		// wait for start to finish // read of SR1 clears the flag
	I2C3->DR = STMPE811_ADDRESS;			// transfer address (Transmit)
	while(!(I2C3->SR1 & I2C_SR1_ADDR));		// wait for address transfer
	dummy = I2C3->SR2;						// clear the flag

	while(!(I2C3->SR1 & I2C_SR1_TXE));		// wait for DR empty
	I2C3->DR = addressRegister;

	while(!(I2C3->SR1 & I2C_SR1_BTF));		// wait for bus not-busy

	I2C3->CR1 |= I2C_CR1_START;				// request a start
	while(!(I2C3->SR1 & I2C_SR1_SB));		// wait for start to finish // read of SR1 clears the flag
	I2C3->DR = STMPE811_ADDRESS|0x01;		// transfer address (Receive)
	while(!(I2C3->SR1 & I2C_SR1_ADDR));		// wait for address transfer
	dummy = I2C3->SR2;						// clear the flag

	while(length)							// transfer whole block
	{
		if(length == 1)	I2C3->CR1 &= ~I2C_CR1_ACK;

		while(!(I2C3->SR1 & I2C_SR1_RXNE));	// wait for DR fill
		*( data++ ) = I2C3->DR;				// receive one byte, increment pointer

		length--;
	}

	I2C3->CR1 |= I2C_CR1_STOP;				// request a stop
}


void I2C_Init (void)
{
	RCC->AHB1ENR |= STMP811_GPIO_DATA_EN;
	RCC->APB1ENR |= RCC_APB1ENR_I2C3EN;
	__DSB();
	gpio_pin_cfg(STMPE811_SCL_GPIO, STMPE811_SCL, GPIO_AF4_OD_50MHz_PULL_UP);
	gpio_pin_cfg(STMPE811_SDA_GPIO, STMPE811_SDA, GPIO_AF4_OD_50MHz_PULL_UP);

	//INICJALIZACJA I2C

	//Software reset
	I2C3->CR1 |=  I2C_CR1_PE;		//software reset
	I2C3->CR1 &= ~I2C_CR1_PE;		//software reset
	while(I2C3->CR1 & I2C_CR1_PE);	//software reset

	I2C3->CR2 = 45; 		//frequency
	I2C3->CCR = 178;		// setup speed (100kHz)
	I2C3->TRISE = 37;		// limit slope
	I2C3->CR1 = I2C_CR1_PE;// enable
}

void EXTI_Init (void)
{
	RCC->AHB1ENR |= STMP811_GPIO_INT_EN;
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
	__DSB();
	gpio_pin_cfg(STMPE811_INT_GPIO, STMPE811_INT, GPIO_IN_PULL_UP);

	SYSCFG->EXTICR[3] |= SYSCFG_EXTICR4_EXTI15_PA;

	EXTI->FTSR |= EXTI_FTSR_TR15;
	EXTI->RTSR |= EXTI_RTSR_TR15;
	EXTI->IMR |= EXTI_IMR_MR15;

	NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void EXTI15_10_IRQHandler(void)
{
	if ( EXTI->PR & EXTI_PR_PR15)
	{
		EXTI->PR = EXTI_PR_PR15;

		uint8_t cRegVal;

		STMPE811_ReadRegisters(STMPE811_INT_STA, &cRegVal,1);

		//sa dane w FIFO - odczytaj pozycje
		if (cRegVal&0x02)	STMPE811_ReadXY();

		//opuszczenie lub podniesienie pisaka
		if (cRegVal&0x01)
		{
			if(!TouchPad.delay)	TouchPad.pressed = 1;
			else				TouchPad.pressed = 0;
		}

		//kasowanie przerwan w ukladzie
		if ((cRegVal&0x03)==3)	STMPE811_WriteRegister(STMPE811_INT_STA, 0x03);
		else if(cRegVal&0x01)	STMPE811_WriteRegister(STMPE811_INT_STA, 0x01);
		else if(cRegVal&0x02)	STMPE811_WriteRegister(STMPE811_INT_STA, 0x02);

	}
}

STMPE811_State_t STMPE811_Init(STMPE811_Orientation_t orientation)
{
	uint8_t Buffer[2]={0};

	/* Initialize I2C */
	I2C_Init();
	/* Initialize Pin Interrupt */
	EXTI_Init();

	//zerowanie ukladu
	STMPE811_WriteRegister(STMPE811_SYS_CTRL1, 0x02);

	//sygnaly taktujace: cz. temperatury - ON, GPIO - OFF,
	//kontroler panelu dotykowego - ON , ADC - ON
	STMPE811_WriteRegister(STMPE811_SYS_CTRL2, 0x04);

	//wlaczenie przerwan: przepelnienie FIFO, dotyk
	STMPE811_WriteRegister(STMPE811_INT_EN, 0x03);

	//czas konwersji ADC = 80 taktow, tryb 12-bit,
	//wewnetrzne zrodlo napiecia odniesienia
	STMPE811_WriteRegister(STMPE811_ADC_CTRL1, 0x49);

	Delayms(5);

	//taktowanie ADC = 3.25MHz
	STMPE811_WriteRegister(STMPE811_ADC_CTRL2, 0x01);

	//alternatywne funkcje GPIO - wylaczone
	STMPE811_WriteRegister(STMPE811_GPIO_AF, 0x00);

	//usrednianie pomiarow � 4 probki; opoznienie wykrycia dotyku � 1ms,
	//czas ustalania pradu sterujacego � 1ms
	STMPE811_WriteRegister(STMPE811_TSC_CFG, 0xA3);

	//prog FIFO = 1
	STMPE811_WriteRegister(STMPE811_FIFO_TH, 0x01);

	//reset FIFO
	STMPE811_WriteRegister(STMPE811_FIFO_STA, 0x01);
	STMPE811_WriteRegister(STMPE811_FIFO_STA, 0x00);

	//Format danych osi Z = z,zzzzzzz
	STMPE811_WriteRegister(STMPE811_TSC_FRACTION_Z, 0x07);

	//prad sterujacy liniami panelu � 50mA
	STMPE811_WriteRegister(STMPE811_TSC_I_DRIVE, 0x01);

	//wlacz panel, pomiar XY
	STMPE811_WriteRegister(STMPE811_TSC_CTRL, 0x03);

	//wyczysc zgloszenia przerwan
	STMPE811_WriteRegister(STMPE811_INT_STA, 0xFF);

	//wlacz przerwania
	STMPE811_WriteRegister(STMPE811_INT_CTRL, 0x01);

	//kontrolny odczyt ID ukladu � powinno byc 0x0811
	STMPE811_ReadRegisters(STMPE811_CHIP_ID,Buffer,2);

	 int16_t RegVal = Buffer[0]<<8|Buffer[1];
	 if (RegVal == 0x0811) return STMPE811_State_Ok;
	 return STMPE811_State_Error;
}

void STMPE811_ReadXY(void)
{
	uint8_t Buffer[3];

	//wartosci XY
	STMPE811_ReadRegisters(STMPE811_TSC_DATA_XYZ, Buffer, 3);

	TouchPad.y = (uint16_t)(Buffer[0]<<4)|(Buffer[1]>>4);
	TouchPad.x = (uint16_t)((Buffer[1]&0x0F)<<8)|Buffer[2];

	if(TouchPad.x<400)	TouchPad.x = 400;
	TouchPad.x -= 400;
	TouchPad.x /= 11;

	if(TouchPad.y<250)	TouchPad.x = 250;
	TouchPad.y -= 250;
	TouchPad.y /= 15;
}















