/*
 * stmpe811.h
 *
 *  Created on: Sep 2, 2016
 *      Author: BloedeBleidd
 */

#ifndef STMPE811_H_
#define STMPE811_H_

#include "../../../CMSIS/stm32f4xx.h"
#include "../../../OS/SystemTick/SysTick.h"
#include "../../../Hardware/GPIO/gpio.h"

/*
STMPE811	STM32F4xx	Description
SCL			PA8			Clock pin for I2C
SDA			PC9			Data pin for I2C
INT		    PA15		Int pin for STMPE811
*/

#define STMPE811_SCL 		8
#define STMPE811_SCL_GPIO	GPIOA
#define STMPE811_SDA 		9
#define STMPE811_SDA_GPIO	GPIOC
#define STMPE811_INT		15
#define STMPE811_INT_GPIO	GPIOA

#define STMP811_GPIO_DATA_EN	(RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOCEN)
#define STMP811_GPIO_INT_EN		(RCC_AHB1ENR_GPIOAEN)

#define STMPE811_I2C		I2C3

/* Default I2C clock for STMPE811 */
#define STMPE811_I2C_CLOCK	100000


/****************************** defgroup TM_STMPE811_Typedefs ******************************/

/* brief  Enum for set how to read x and y from controller */

typedef enum
{
	STMPE811_Orientation_Portrait_1,	// Portrait orientation mode 1
	STMPE811_Orientation_Portrait_2,	// Portrait orientation mode 2
	STMPE811_Orientation_Landscape_1,	// Landscape orientation mode 1
	STMPE811_Orientation_Landscape_2,	// Landscape orientation mode 2
} STMPE811_Orientation_t;


/* Enumeration for touch pressed or released */

typedef enum
{
	STMPE811_State_Ok,       	// Result OK. Used on initialization
	STMPE811_State_Error,    	// Result error. Used on initialization
	STMPE811_State_Pressed,		// Touch detected as pressed
	STMPE811_State_Released		// Touch detected as released/not pressed
} STMPE811_State_t;


/* Main structure, which is passed into STMPE811_ReadTouch() function */

typedef struct
{
	uint16_t x;							// X coordinate on LCD for touch */
	uint16_t y;							// Y coordinate on LCD for touch */
	uint16_t delay;
	STMPE811_State_t pressed;           // Pressed touch status */
	STMPE811_Orientation_t orientation;	// Touch screen orientation to match your LCD orientation */
} STMPE811_Data_t;


/****************************** defgroup TM_STMPE811_DataStructure ******************************/

extern volatile STMPE811_Data_t TouchPad;

/****************************** defgroup TM_STMPE811_Functions ******************************/

STMPE811_State_t STMPE811_Init(STMPE811_Orientation_t orientation);
void STMPE811_Timer_Process (void);



/******************** Checks if touch data is inside specific rectangle coordinates ********************/

#define STMPE811_TouchInRectangle(sd, xPos, yPos, w, h)	(((sd)->x >= (xPos)) && ((sd)->x < (xPos + w)) && ((sd)->y >= (yPos)) && ((sd)->y < (yPos + h)))


#endif /* STMPE811_H_ */
