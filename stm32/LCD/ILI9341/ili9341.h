/*
 * ili9341.h
 *
 *  Created on: Aug 21, 2016
 *      Author: BloedeBleidd
 */

#ifndef ILI9341_H_
#define ILI9341_H_

#include "../../../CMSIS/stm32f4xx.h"
#include "ili9341.h"
#include "fonts.h"

/**
 * Select font
 */
extern FontDef_t Font_7x10;
extern FontDef_t Font_11x18;
extern FontDef_t Font_16x26;
extern FontDef_t Font_16x24;


//LCD settings
#define LCD_WIDTH     240
#define LCD_HEIGHT    320
#define LCD_PIXEL    76800

/*
 * Orientation
 * Used private
 */
typedef enum {
	LCD_Landscape,
	LCD_Portrait
} LCD_Orientation;

/*
 * LCD options
 * Used private
 */
typedef struct
{
	uint16_t Width;
	uint16_t Height;
	LCD_Orientation orientation; // 1 = portrait; 0 = landscape
} LCD_Options_t;

/*
 * Public enum
 * Select orientation for LCD
 */
typedef enum
{
	LCD_Orientation_Portrait_1,
	LCD_Orientation_Portrait_2,
	LCD_Orientation_Landscape_1,
	LCD_Orientation_Landscape_2
} LCD_Orientation_t;


extern LCD_Options_t LCD_Opts;

/****************************** ILI9341 commands ******************************/

#define LCD_SOFT_RES			0x01	// Software reset
#define LCD_SLEEP_OUT			0x11	// Sleep out register
#define LCD_GAMMA				0x26	// Gamma register
#define LCD_DISPLAY_OFF			0x28	// Display off register
#define LCD_DISPLAY_ON			0x29	// Display on register
#define LCD_COLUMN_ADDR			0x2A	// Colomn address register
#define LCD_PAGE_ADDR			0x2B	// Page address register
#define LCD_GRAM				0x2C	// GRAM register
#define LCD_MAC					0x36	// Memory Access Control register
#define LCD_PIXEL_FORMAT		0x3A	// Pixel Format register
#define LCD_WDB					0x51	// Write Brightness Display register
#define LCD_WCD					0x53	// Write Control Display register
#define LCD_RGB_INTERFACE		0xB0	// RGB Interface Signal Control
#define	LCD_FRAME_CONT_NORM		0xB1	// Frame Control (In Normal Mode)
#define LCD_FRC					0xB2	// Frame Rate Control register
#define LCD_BPC					0xB5	// Blanking Porch Control register
#define LCD_DFC					0xB6	// Display Function Control register
#define	LCD_ENT_MODE_SET		0xB7	// Entry Mode Set
#define LCD_POWER1				0xC0	// Power Control 1 register
#define LCD_POWER2				0xC1	// Power Control 2 register
#define LCD_VCOM1				0xC5	// VCOM Control 1 register
#define LCD_VCOM2				0xC7	// VCOM Control 2 register
#define LCD_POWERA				0xCB	// Power control A register
#define LCD_POWERB				0xCF	// Power control B register
#define LCD_PGAMMA				0xE0	// Positive Gamma Correction register
#define LCD_NGAMMA				0xE1	// Negative Gamma Correction register
#define LCD_DTCA				0xE8	// Driver timing control A
#define LCD_DTCB				0xEA	// Driver timing control B
#define LCD_POWER_SEQ			0xED	// Power on sequence register
#define LCD_3GAMMA_EN			0xF2	// 3 Gamma enable register
#define LCD_INTERFACE			0xF6	// Interface control register
#define LCD_PRC					0xF7	// Pump ratio control register


/****************************** ILI9341 communication macro ******************************/

#define	CS_SELECT			GPIOC->ODR &= ~GPIO_ODR_ODR_2
#define	CS_DESELECT			GPIOC->ODR |= GPIO_ODR_ODR_2

#define	DATA_MODE			GPIOD->ODR |= GPIO_ODR_ODR_13
#define	CMD_MODE			GPIOD->ODR &= ~GPIO_ODR_ODR_13

#define SPI_16BIT_MODE	SPI5->CR1 |= SPI_CR1_DFF
#define SPI_8BIT_MODE	SPI5->CR1 &= ~SPI_CR1_DFF

#define	SPI_DATA				SPI5->DR
#define	SPI_BUSY_FLAG			SPI5->SR & SPI_SR_BSY
#define SPI_WAIT_FOR_BUSY_FLAG	while((SPI_BUSY_FLAG))


extern uint16_t ILI9341_x;
extern uint16_t ILI9341_y;

/****************************** Settings ******************************/

void LCD_Init(LCD_Orientation_t orientation);
void LCD_Rotate(LCD_Orientation_t orientation);

/****************************** Graphics ******************************/

void Set_Window( uint16_t x_start, uint16_t x_stop, uint8_t y_start, uint8_t y_stop );
void Fill_Window( uint32_t size, uint16_t color );

void Draw_Window( uint16_t x_start, uint16_t x_stop, uint8_t y_start, uint8_t y_stop, uint16_t color );
void Print_Char(uint16_t x, uint16_t y, char c, FontDef_t *font, uint16_t color, uint16_t background);
void Print_String(uint16_t x, uint16_t y, char *str, FontDef_t *font, uint16_t color, uint16_t background);
void Print_Variable(uint16_t x, uint16_t y, int32_t var, uint8_t type, FontDef_t *font, uint16_t color, uint16_t background);

/****************************** Hardware ******************************/

void SPI_Write8 (uint8_t data);
void SPI_Write16 (uint16_t data);
void write_cmd( uint8_t data );
void write_data( uint8_t data );
void LCD_Delay(uint16_t del);


#endif /* ILI9341_H_ */
