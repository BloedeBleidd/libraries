/*
 * ili9341.c
 *
 *  Created on: Aug 21, 2016
 *      Author: BloedeBleidd
 */

#include "../../../Hardware/GPIO/gpio.h"
#include "../../../OS/SystemTick/SysTick.h"
#include "ili9341.h"

LCD_Options_t LCD_Opts;


void LCD_SPI_Init (void)
{
	//---------------------------------------GPIO Settings------------------------------------------------
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN | RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIODEN;
	__DSB();
	gpio_pin_cfg(GPIOC, 2, GPIO_OUT_PP_100MHz);	//CS
	gpio_pin_cfg(GPIOD, 13, GPIO_OUT_PP_100MHz); //DC
	gpio_pin_cfg(GPIOF, 7, GPIO_AF5_PP_100MHz);  //CLK
	gpio_pin_cfg(GPIOF, 9, GPIO_AF5_PP_100MHz);  //MOSI

	//---------------------------------------SPI Settings------------------------------------------------
	RCC->APB2ENR |= RCC_APB2ENR_SPI5EN;
	__DSB();
	SPI5->CR1 = //SPI_CR1_BR_0 |
				SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_SPE;
}

void SPI_Write8 (uint8_t data)
{
	SPI_DATA = data;
	SPI_WAIT_FOR_BUSY_FLAG;
	for(uint32_t temp=0; temp<7; temp++){ asm ("nop");}
}

void SPI_Write16 (uint16_t data)
{
	SPI_DATA = data;
	SPI_WAIT_FOR_BUSY_FLAG;
}

void write_cmd( uint8_t data )
{
	CMD_MODE;
	CS_SELECT;
	SPI_Write8(data);
	CS_DESELECT;
}

void write_data( uint8_t data )
{
	DATA_MODE;
	CS_SELECT;
	SPI_Write8(data);
	CS_DESELECT;
}

void LCD_Delay(uint16_t del)
{
	Program_Timer[0] = del;
	while(Program_Timer[0]);
}

void LCD_Init(LCD_Orientation_t orientation)
{
	LCD_SPI_Init();

	//Start initial Sequence
	write_cmd(LCD_SOFT_RES); //software reset
	LCD_Delay(50);
	write_cmd(LCD_DISPLAY_OFF); // display off

	write_cmd(LCD_POWERA);
	write_data(0x39);
	write_data(0x2C);
	write_data(0x00);
	write_data(0x34);
	write_data(0x02);
	write_cmd(LCD_POWERB);
	write_data(0x00);
	write_data(0xC1);
	write_data(0x30);
	write_cmd(LCD_DTCA);
	write_data(0x85);
	write_data(0x00);
	write_data(0x78);
	write_cmd(LCD_DTCB);
	write_data(0x00);
	write_data(0x00);
	write_cmd(LCD_POWER_SEQ);
	write_data(0x64);
	write_data(0x03);
	write_data(0x12);
	write_data(0x81);
	write_cmd(LCD_PRC);
	write_data(0x20);
	write_cmd(LCD_POWER1);
	write_data(0x23);
	write_cmd(LCD_POWER2);
	write_data(0x10);
	write_cmd(LCD_VCOM1);
	write_data(0x3E);
	write_data(0x28);
	write_cmd(LCD_VCOM2);
	write_data(0x86);
	write_cmd(LCD_MAC);
	write_data(0x48);
	write_cmd(LCD_PIXEL_FORMAT);
	write_data(0x55);
	write_cmd(LCD_FRC);
	write_data(0x00);
	write_data(0x18);
	write_cmd(LCD_DFC);
	write_data(0x08);
	write_data(0x82);
	write_data(0x27);
	write_cmd(LCD_3GAMMA_EN);
	write_data(0x00);
	write_cmd(LCD_COLUMN_ADDR);
	write_data(0x00);
	write_data(0x00);
	write_data(0x00);
	write_data(0xEF);
	write_cmd(LCD_PAGE_ADDR);
	write_data(0x00);
	write_data(0x00);
	write_data(0x01);
	write_data(0x3F);
	write_cmd(LCD_GAMMA);
	write_data(0x01);
	write_cmd(LCD_PGAMMA);
	write_data(0x0F);
	write_data(0x31);
	write_data(0x2B);
	write_data(0x0C);
	write_data(0x0E);
	write_data(0x08);
	write_data(0x4E);
	write_data(0xF1);
	write_data(0x37);
	write_data(0x07);
	write_data(0x10);
	write_data(0x03);
	write_data(0x0E);
	write_data(0x09);
	write_data(0x00);
	write_cmd(LCD_NGAMMA);
	write_data(0x00);
	write_data(0x0E);
	write_data(0x14);
	write_data(0x03);
	write_data(0x11);
	write_data(0x07);
	write_data(0x31);
	write_data(0xC1);
	write_data(0x48);
	write_data(0x08);
	write_data(0x0F);
	write_data(0x0C);
	write_data(0x31);
	write_data(0x36);
	write_data(0x0F);

	write_cmd(0x11); //sleep out
	LCD_Delay(120);
	write_cmd(0x29); // display on
	LCD_Delay(100);

	LCD_Rotate(orientation);

	//Please add code write_cmd(0x2C); to access the GRAM before to display picture.

	Draw_Window(0,319,0,239,0);
}

void LCD_Rotate(LCD_Orientation_t orientation)
{
	write_cmd(LCD_MAC);
	if (orientation == LCD_Orientation_Portrait_1)			write_data(0x58);
	else if (orientation == LCD_Orientation_Portrait_2)		write_data(0x88);
	else if (orientation == LCD_Orientation_Landscape_1)	write_data(0x28);
	else if (orientation == LCD_Orientation_Landscape_2)	write_data(0xE8);

	if (orientation == LCD_Orientation_Portrait_1 || orientation == LCD_Orientation_Portrait_2)
	{
		LCD_Opts.Width = LCD_WIDTH;
		LCD_Opts.Height = LCD_HEIGHT;
		LCD_Opts.orientation = LCD_Portrait;
	}
	else
	{
		LCD_Opts.Width = LCD_HEIGHT;
		LCD_Opts.Height = LCD_WIDTH;
		LCD_Opts.orientation = LCD_Landscape;
	}
}
