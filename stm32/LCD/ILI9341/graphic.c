/*
 * graphic.c
 *
 *  Created on: Aug 24, 2016
 *      Author: BloedeBleidd
 */

#include <stdint.h>
#include <stdlib.h>
#include "ili9341.h"
#include "fonts.h"

uint16_t ILI9341_x;
uint16_t ILI9341_y;

static void __reverse(char* begin,char* end)
{
	char temp;

	while (end>begin)
	{
		temp=*end;
		*end--=*begin;
		*begin++=temp;
	}
}

char* long_to_ascii(int32_t value, char* buffer, uint8_t base)
{
	static const char digits[]="0123456789abcdef";

	char* buffer_copy=buffer;
	int32_t sign=0;
	int32_t quot,rem;

	if ((base>=2)&&(base<=16))				// is the base valid?
	{
		if (base == 10 && (sign = value) < 0)// negative value and base == 10? store the copy (sign)
			value = -value;					// make it positive

		do
		{
			quot=value/base;				// calculate quotient and remainder
			rem=value%base;
			*buffer++ = digits[rem];		// append the remainder to the string
		}
		while ((value=quot));				// loop while there is something to convert

		if (sign<0)							// was the value negative?
			*buffer++='-';					// append the sign

		__reverse(buffer_copy,buffer-1);		// reverse the string
	}

	*buffer='\0';
	return buffer_copy;
}

void Set_Window( uint16_t x_start, uint16_t x_stop, uint8_t y_start, uint8_t y_stop )
{
	write_cmd( LCD_COLUMN_ADDR );
		write_data( x_start>>8 );
		write_data( x_start & 0xFF );
		write_data( x_stop>>8 );
		write_data( x_stop & 0xFF);
	write_cmd( LCD_PAGE_ADDR );
		write_data( y_start>>8 );
		write_data( y_start & 0xFF);
		write_data( y_stop>>8 );
		write_data( y_stop & 0xFF);
	write_cmd( LCD_GRAM );
}

void Fill_Window( uint32_t size, uint16_t color )
{
	SPI_16BIT_MODE;
	DATA_MODE;
	CS_SELECT;
	while( size-- )	SPI_Write16(color);
	CS_DESELECT;
	SPI_8BIT_MODE;
}

void Draw_Window( uint16_t x_start, uint16_t x_stop, uint8_t y_start, uint8_t y_stop, uint16_t color )
{
	Set_Window( x_start, x_stop, y_start, y_stop );
	uint32_t i = ((x_stop-x_start)+1)*((y_stop-y_start)+1);	// obliczanie liczby pikseli do wypelnienia
	Fill_Window( i, color );
}

void Print_Char(uint16_t x, uint16_t y, char c, FontDef_t *font, uint16_t color, uint16_t background)
{
	 ILI9341_x = x;
	 ILI9341_y = y;

	 if ((ILI9341_x + font->FontWidth) > LCD_Opts.Width)
	 {
		 //If at the end of a line of display, go to new line and set x to 0 position
		 ILI9341_y += font->FontHeight;
		 ILI9341_x = 0;
	 }

	 Set_Window( ILI9341_x, ILI9341_x+font->FontWidth-1, ILI9341_y, ILI9341_y+font->FontHeight-1 );
	 SPI_16BIT_MODE;
	 DATA_MODE;
	 CS_SELECT;
	 for(uint16_t i=0; i<font->FontHeight; i++ )
	 {
		 uint16_t elem = font->data[(c - 32) * font->FontHeight + i];
		 for(uint16_t j=0; j<font->FontWidth; j++ )
		 {
			 uint16_t col;
			 if ((elem << j) & 0x8000)	col = color;
			 else 						col = background;
			 SPI_Write16(col);
		 }
	}
	CS_DESELECT;
	SPI_8BIT_MODE;

	ILI9341_x += font->FontWidth;
}

void Print_String(uint16_t x, uint16_t y, char *str, FontDef_t *font, uint16_t color, uint16_t background)
{
	uint16_t startX = x;

	/* Set X and Y coordinates */
	ILI9341_x = x;
	ILI9341_y = y;

	while (*str)
	{
		//New line
		if (*str == '\n')
		{
			ILI9341_y += font->FontHeight + 1;
			//if after \n is also \r, than go to the left of the screen
			if (*(str + 1) == '\r')
			{
				ILI9341_x = 0;
				str++;
			}
			else
			{
				ILI9341_x = startX;
			}

			str++;
			continue;
		}
		else if (*str == '\r')
		{
			str++;
			continue;
		}

		Print_Char(ILI9341_x, ILI9341_y, *str++, font, color, background);
	}
}

void Print_Variable(uint16_t x, uint16_t y, int32_t var, uint8_t type, FontDef_t *font, uint16_t color, uint16_t background)
{
	char buf [35];
	long_to_ascii(var,buf,type);
	Print_String(x,y,buf,font,color,background);
}




















/*
void Print_String( uint16_t x, uint8_t y, char *s, uint16_t color ) // 16x24
{
	uint16_t elem=0, col=0;
	uint8_t i, j;
	uint16_t k, l=0;

	do{
		Set_Window( x+l, x+l+15, y, y+23 );
		k = (*s-0x20)*24;
		SPI_16BIT_MODE;
		DATA_MODE;
		CS_SELECT;
		for( i=0; i<24; i++ )
		{
			elem = ASCII16x24_Table[k+i];
			for( j=0; j<16; j++ )
			{
				col = ((elem>>j)&1) ? color:0;
				SPI_Write16(col);
			}
		}
		CS_DESELECT;
	s++;
	l += 16;
	SPI_8BIT_MODE;
	}while( *s );
}

void Print_Variable( uint16_t x, uint8_t y, uint32_t var, uint16_t color  )
{
	uint16_t elem=0, col=0;
	uint8_t i, j;

	Set_Window( x, x+15, y, y+23 );
	var += 0x10;
	SPI_WAIT_FOR_BUSY_FLAG;
	CS_DESELECT;
	SPI_16BIT_MODE;
	DATA_MODE;
	CS_SELECT;
	for( i=0; i<24; i++ )
	{
		elem = ASCII16x24_Table[(var*24)+i];
		for( j=0; j<16; j++ )
		{
			col = ((elem>>j)&1) ? color:0;
			SPI_WAIT_FOR_BUSY_FLAG;
			SPI_DATA = col;
		}
	}
	SPI_8BIT_MODE;
}
*/
