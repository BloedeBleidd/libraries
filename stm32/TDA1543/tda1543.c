/*
 * tda1543.c
 *
 *  Created on: Nov 6, 2016
 *      Author: BloedeBleidd
 */

#include "tda1543.h"

typedef enum { left=0, right=1 }ws_ctrl_t;

void ws_control (ws_ctrl_t state)
{
	I2S_SPI_WAIT_FOR_BUSY_FLAG;
	if(state==left)
	{
		I2S_GPIOx->ODR &= ~I2S_WS_PIN;
	}
	else
	{
		I2S_GPIOx->ODR |= I2S_WS_PIN;
	}
}


void tda1543_init_i2s (void)
{
	// init pins
	gpio_pin_config(I2S_GPIOx, I2S_WS_PIN, gpio_mode_output_PP_50MHz);		//ws
	gpio_pin_config(I2S_GPIOx, I2S_SD_PIN, gpio_mode_alternate_PP_50MHz);	//serial data
	gpio_pin_config(I2S_GPIOx, I2S_CLK_PIN, gpio_mode_alternate_PP_50MHz);	//serial clock

	// init spi
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;

	I2S_SPIx->CR1 = SPI_CR1_BR_2 |SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_DFF | SPI_CR1_CPHA;
	I2S_SPIx->CR1 |= SPI_CR1_SPE;

	ws_control(left);

}


static void send_data (tda1543_channel_data_t data)
{
	I2S_SPI_WAIT_FOR_TXE_FLAG;
	I2S_SPI_DATA = data;
}

void tda1543_send_package (tda1543_data_t data)
{
	ws_control(left);
	send_data(data.left);
	ws_control(right);
	send_data(data.right);
}
