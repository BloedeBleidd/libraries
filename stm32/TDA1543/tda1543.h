/*
 * tda1543.h
 *
 *  Created on: Nov 6, 2016
 *      Author: BloedeBleidd
 */

#ifndef TDA1543_TDA1543_H_
#define TDA1543_TDA1543_H_

#include "stm32f10x.h"
#include "../GPIO/gpio.h"

#define I2S_SPIx 		SPI1
#define I2S_GPIOx 		GPIOA
#define I2S_WS_PIN		Pin4
#define I2S_CLK_PIN		Pin5
#define I2S_SD_PIN		Pin7

#define I2S_SPI_DATA				I2S_SPIx->DR

#define I2S_SPI_TXE_FLAG			I2S_SPIx->SR & SPI_SR_TXE
#define I2S_SPI_WAIT_FOR_TXE_FLAG	while(!(I2S_SPI_TXE_FLAG))

#define	I2S_SPI_BUSY_FLAG			I2S_SPIx->SR & SPI_SR_BSY
#define I2S_SPI_WAIT_FOR_BUSY_FLAG	while(I2S_SPI_BUSY_FLAG)

//data type
typedef int16_t	tda1543_channel_data_t;

typedef struct
{
	tda1543_channel_data_t left;
	tda1543_channel_data_t right;
}tda1543_data_t;

void tda1543_init_i2s (void);
void tda1543_send_package (tda1543_data_t);

#endif /* TDA1543_TDA1543_H_ */
