/*
 * criticalsection.h
 *
 *  Created on: Dec 12, 2016
 *      Author: BloedeBleidd
 */

#ifndef CRITICALSECTION_CRITICALSECTION_H_
#define CRITICALSECTION_CRITICALSECTION_H_



#define CriticalSectionVar()  uint8_t cpuSR

#define CriticalSectionEnter()		\
do									\
{									\
    asm								\
	(								\
    "MRS   R0, PRIMASK\n\t"			\
    "CPSID I\n\t"					\
    "STRB R0, %[output]"			\
    : [output] "=m" (cpuSR) :: "r0"	\
	);								\
} while(0)

#define CriticalSectionExit()		\
do									\
{									\
	asm								\
	(								\
    "ldrb r0, %[input]\n\t"			\
    "msr PRIMASK,r0;\n\t"			\
    ::[input] "m" (cpuSR) : "r0"	\
	 );								\
} while(0)



#endif /* CRITICALSECTION_CRITICALSECTION_H_ */
