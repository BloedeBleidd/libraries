/*
 * gpio.c
 *
 *  Created on: 09.06.2017
 *      Author: BloedeBleidd
 */


#include "gpio.h"

void gpioInitialize(void)
{
	// enable all possible GPIO ports
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN | RCC_AHBENR_GPIOFEN;
}

GpioReturn gpioPinConfiguration(GPIO_TypeDef * const port, uint8_t pin, GpioMode mode)
{
	uint32_t tmp;

	if(pin>=16)	return GPIO_ERROR;

	tmp = port->MODER;
	tmp &= ~(0x03 << pin*2);
	tmp |= (((mode>>5) & 0x03) << (pin*2));
	port->MODER = tmp;

	tmp = port->OTYPER;
	tmp &= ~(0x01 << pin);
	tmp |= (((mode>>4) & 0x01) << (pin*1));
	port->OTYPER = tmp;

	tmp = port->OSPEEDR;
	tmp &= ~(0x03 << pin*2);
	tmp |= (((mode>>2) & 0x03) << (pin*2));
	port->OSPEEDR = tmp;

	tmp = port->PUPDR;
	tmp &= ~(0x03 << pin*2);
	tmp |= (((mode>>0) & 0x03) << (pin*2));
	port->PUPDR = tmp;

	return GPIO_OK;
}

GpioReturn gpioPinAlternateFunctionConfiguration(GPIO_TypeDef * const port, uint8_t pin, uint8_t af)
{
	uint32_t tmp;
	uint8_t i = 0;

	if(pin>=16)	return GPIO_ERROR;

	if(pin>=8)
	{
		pin -= 8;
		i = 1;
	}
	pin *= 4;

	tmp = port->AFR[i];
	tmp |= af << pin;
	port->AFR[i] = tmp;

	return GPIO_OK;
}


void gpioLockConfiguration(GPIO_TypeDef* GPIOx, uint16_t gpio_pin)
{
  uint32_t tmp = 0x00010000;

  tmp |= gpio_pin;
  /* Set LCKK bit */
  GPIOx->LCKR = tmp;
  /* Reset LCKK bit */
  GPIOx->LCKR =  gpio_pin;
  /* Set LCKK bit */
  GPIOx->LCKR = tmp;
  /* Read LCKK bit*/
  tmp = GPIOx->LCKR;
  /* Read LCKK bit*/
  tmp = GPIOx->LCKR;
}

void gpioBitSet(GPIO_TypeDef* GPIOx, uint8_t gpio_pin)
{
  GPIOx->BSRR = 1 << gpio_pin;
}

void gpioBitReset(GPIO_TypeDef* GPIOx, uint8_t gpio_pin)
{
  GPIOx->BRR = 1 << gpio_pin;
}

void gpioBitToggle(GPIO_TypeDef* GPIOx, uint8_t gpio_pin)
{
  GPIOx->ODR ^= 1 << gpio_pin;
}

uint8_t gpioBitRead(GPIO_TypeDef* GPIOx, uint16_t gpio_pin)
{
  uint8_t bitStatus = 0x00;

  if ((GPIOx->IDR & (1 << gpio_pin)) == (uint8_t)bitSet)
  {
    bitStatus = (uint8_t)bitSet;
  }
  else
  {
    bitStatus = (uint8_t)bitReset;
  }

  return bitStatus;
}


void gpioPortWrite(GPIO_TypeDef* GPIOx, uint16_t value)
{
  GPIOx->ODR = value;
}

uint16_t gpioPortRead(GPIO_TypeDef* GPIOx)
{
  return ((uint16_t)GPIOx->IDR);
}
