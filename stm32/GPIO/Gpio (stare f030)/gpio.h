/*
 * gpio.h
 *
 *  Created on: 09.06.2017
 *      Author: BloedeBleidd
 */

#ifndef GPIO_GPIO_H_
#define GPIO_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f0xx.h"


typedef enum
{
	bitSet=1,
	bitReset=0,
}setReset;

typedef enum
{
	GPIO_OK=1,
	GPIO_ERROR=0,
}GpioReturn;

typedef enum
{
	/* Push-Pull */
	gpio_mode_output_PP_2MHz		= 0b0100000,
	gpio_mode_output_PP_10MHz		= 0b0100100,
	gpio_mode_output_PP_50MHz		= 0b0101100,
	/* Open-Drain */
	gpio_mode_output_OD_2MHz		= 0b0110000,
	gpio_mode_output_OD_10MHz		= 0b0110100,
	gpio_mode_output_OD_50MHz		= 0b0111100,
	/* Push-Pull */
	gpio_mode_alternate_PP_2MHz		= 0b1000000,
	gpio_mode_alternate_PP_10MHz	= 0b1000100,
	gpio_mode_alternate_PP_50MHz	= 0b1001100,
	/* Open-Drain */
	gpio_mode_alternate_OD_2MHz		= 0b1010000,
	gpio_mode_alternate_OD_10MHz	= 0b1010100,
	gpio_mode_alternate_OD_50MHz	= 0b1011100,
	/* Digital input with pull-up/down */
	gpio_mode_input_pull_down		= 0b0000010,
	gpio_mode_input_pull_up			= 0b0000001,
	/* Floating digital input. */
	gpio_mode_input_floating		= 0b0000000,
	/* Analog input (ADC) */
	gpio_mode_input_analog			= 0b1100000,
} GpioMode;


void gpioInitialize(void);
GpioReturn gpioPinConfiguration(GPIO_TypeDef * const port, uint8_t pin, GpioMode mode);
GpioReturn gpioPinAlternateFunctionConfiguration(GPIO_TypeDef * const port, uint8_t pin, uint8_t af);
void gpioLockConfiguration(GPIO_TypeDef*, uint16_t);

void gpioBitSet(GPIO_TypeDef*, uint8_t);
void gpioBitReset(GPIO_TypeDef*, uint8_t);
void gpioBitToggle(GPIO_TypeDef*, uint8_t);
uint8_t gpioBitRead(GPIO_TypeDef*, uint16_t);

void gpioPortWrite(GPIO_TypeDef*, uint16_t);
uint16_t gpioPortRead(GPIO_TypeDef*);

#ifdef __cplusplus
}
#endif

#endif /* GPIO_GPIO_H_ */
