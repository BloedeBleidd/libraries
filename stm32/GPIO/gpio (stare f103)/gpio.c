/*
 * gpio.c
 *
 *  Created on: Nov 2, 2016
 *      Author: BloedeBleidd
 */


#include "gpio.h"


void gpioInitialize(void)
{
	// enable all possible GPIO ports
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN;
}

void gpioPinConfiguration(GPIO_TypeDef * const port, PinNumber pin, GpioMode mode)
{
	pin = __builtin_ctz(pin)*4;

	uint32_t volatile * cr_reg;
	uint32_t cr_val;

	cr_reg = &port->CRL;

	if (pin > 28)
	{
		pin -= 32;
		cr_reg = &port->CRH;
	}

	cr_val = *cr_reg;
	cr_val &= ~((uint32_t)(0x0f << pin));
	cr_val |= (uint32_t)(mode << pin);
	*cr_reg = cr_val;
}

void gpioLockConfiguration(GPIO_TypeDef* GPIOx, uint16_t gpio_pin)
{
  uint32_t tmp = 0x00010000;

  tmp |= gpio_pin;
  /* Set LCKK bit */
  GPIOx->LCKR = tmp;
  /* Reset LCKK bit */
  GPIOx->LCKR =  gpio_pin;
  /* Set LCKK bit */
  GPIOx->LCKR = tmp;
  /* Read LCKK bit*/
  tmp = GPIOx->LCKR;
  /* Read LCKK bit*/
  tmp = GPIOx->LCKR;
}

void gpioBitSet(GPIO_TypeDef* GPIOx, PinNumber gpio_pin)
{
  GPIOx->BSRR = gpio_pin;
}

void gpioBitReset(GPIO_TypeDef* GPIOx, PinNumber gpio_pin)
{
  GPIOx->BRR = gpio_pin;
}

void gpioBitToggle(GPIO_TypeDef* GPIOx, PinNumber gpio_pin)
{
  GPIOx->ODR ^= gpio_pin;
}

uint8_t gpioBitRead(GPIO_TypeDef* GPIOx, uint16_t gpio_pin)
{
  uint8_t bitStatus = 0x00;

  if ((GPIOx->IDR & gpio_pin) == (uint8_t)bitSet)
  {
    bitStatus = (uint8_t)bitSet;
  }
  else
  {
    bitStatus = (uint8_t)bitReset;
  }

  return bitStatus;
}


void gpioPortWrite(GPIO_TypeDef* GPIOx, uint16_t value)
{
  GPIOx->ODR = value;
}

uint16_t gpioPortRead(GPIO_TypeDef* GPIOx)
{
  return ((uint16_t)GPIOx->IDR);
}




