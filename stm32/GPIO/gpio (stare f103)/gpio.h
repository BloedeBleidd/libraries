/*
 * gpio.h
 *
 *  Created on: Nov 2, 2016
 *      Author: BloedeBleidd
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef GPIO_GPIO_H_
#define GPIO_GPIO_H_

#include "stm32f10x.h"

typedef enum
{
	bitSet=1,
	bitReset=0,
}setReset;

typedef enum {
	Pin0 = 0x00000001,
	Pin1 = 0x00000002,
	Pin2 = 0x00000004,
	Pin3 = 0x00000008,
	Pin4 = 0x00000010,
	Pin5 = 0x00000020,
	Pin6 = 0x00000040,
	Pin7 = 0x00000080,
	Pin8 = 0x00000100,
	Pin9 = 0x00000200,
	Pin10 = 0x00000400,
	Pin11 = 0x00000800,
	Pin12 = 0x00001000,
	Pin13 = 0x00002000,
	Pin14 = 0x00004000,
	Pin15 = 0x00008000
}PinNumber;

typedef enum
{
	/* Push-Pull */
	gpio_mode_output_PP_2MHz = 2,
	gpio_mode_output_PP_10MHz = 1,
	gpio_mode_output_PP_50MHz = 3,
	/* Open-Drain */
	gpio_mode_output_OD_2MHz = 6,
	gpio_mode_output_OD_10MHz = 5,
	gpio_mode_output_OD_50MHz = 7,
	/* Push-Pull */
	gpio_mode_alternate_PP_2MHz = 10,
	gpio_mode_alternate_PP_10MHz = 9,
	gpio_mode_alternate_PP_50MHz = 11,
	/* Open-Drain */
	gpio_mode_alternate_OD_2MHz = 14,
	gpio_mode_alternate_OD_10MHz = 13,
	gpio_mode_alternate_OD_50MHz = 15,
	/* Analog input (ADC) */
	gpio_mode_input_analog = 0,
	/* Floating digital input. */
	gpio_mode_input_floating = 4,
	/* Digital input with pull-up/down (depending on the ODR reg.). */
	gpio_mode_input_pull = 8
} GpioMode;


void gpioInitialize(void);
void gpioPinConfiguration(GPIO_TypeDef * const port, PinNumber, GpioMode);
void gpioLockConfiguration(GPIO_TypeDef*, uint16_t);

void gpioBitSet(GPIO_TypeDef*, PinNumber);
void gpioBitReset(GPIO_TypeDef*, PinNumber);
void gpioBitToggle(GPIO_TypeDef*, PinNumber);
uint8_t gpioBitRead(GPIO_TypeDef*, uint16_t);

void gpioPortWrite(GPIO_TypeDef*, uint16_t);
uint16_t gpioPortRead(GPIO_TypeDef*);

#endif /* GPIO_GPIO_H_ */

#ifdef __cplusplus
}
#endif
