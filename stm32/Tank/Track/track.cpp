/*
 * track.cpp
 *
 *  Created on: 17.01.2017
 *      Author: BloedeBleidd
 */

#include "track.h"
#include <cstdlib>

Track::Track(uint16_t *reg1, uint16_t *reg2)
{
	pwm				= 0;
	direction		= STOP;
	pwmRegister1	= reg1;
	pwmRegister2	= reg2;
}

void Track::setMotion(Direction dir, uint16_t speed)
{
	if(speed>1000)	speed = 1000;
	pwm			= speed;
	direction	= dir;

	if(direction == FORWARD)
	{
		*pwmRegister1 = 0;
		*pwmRegister2 = pwm;
	}
	else if(direction == BACKWARD)
	{
		*pwmRegister2 = 0;
		*pwmRegister1 = pwm;
	}
	else if(direction == STOP)
	{
		*pwmRegister1 = 0;
		*pwmRegister2 = 0;
	}
}

void Track::calculatePwm(Direction &dir, uint16_t &pwm)
{
	if(pwm>1000)
	{
		dir = FORWARD;
		pwm -= 1000;
	}
	else if(pwm<1000)
	{
		dir = BACKWARD;
		pwm = 1000 - pwm;
	}
	else
	{
		dir = STOP;
	}
}

std::string Track::getMotionString()
{
	std::string value,tmp,tmpDirection;
	uint16_t	pwmReg = 0;

	if(direction == FORWARD)
	{
		pwmReg = *pwmRegister2;
		tmpDirection = "forw";
	}
	else if(direction == BACKWARD)
	{
		pwmReg = *pwmRegister1;
		tmpDirection = "back";
	}
	else if(direction == STOP)
	{
		pwmReg = 0;
		tmpDirection = "stop";
	}

	itoa(pwmReg/10,(char*)tmp.c_str(),10);
	value = tmp.c_str();
	value += "% ";
	value += tmpDirection;
	return value;
}
