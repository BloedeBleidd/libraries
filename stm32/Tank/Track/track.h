/*
 * track.h
 *
 *  Created on: 17.01.2017
 *      Author: BloedeBleidd
 */

#ifndef TANK_TRACK_TRACK_H_
#define TANK_TRACK_TRACK_H_

#include "stm32f10x.h"
#include <string>


enum Direction
{
	STOP		= 0,
	BACKWARD	= 1,
	FORWARD		= 2,
};


class Track
{
private:
	uint16_t pwm;
	Direction direction;

	uint16_t *pwmRegister1, *pwmRegister2;

public:
	Track(uint16_t *reg1, uint16_t *reg2);

	void setMotion(Direction, uint16_t);
	void calculatePwm(Direction &dir, uint16_t &pwm);
	std::string getMotionString();
};

#endif /* TANK_TRACK_TRACK_H_ */
