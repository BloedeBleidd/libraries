/*
 * battery.cpp
 *
 *  Created on: 17.01.2017
 *      Author: BloedeBleidd
 */

#include "battery.h"
#include <cstdlib>

Battery::Battery(uint16_t *adcReg)
{
	battery		= 0;
	adcRegister = adcReg;
}

uint16_t Battery::getValue()
{
	return *adcRegister;
}

std::string Battery::getVoltageString()
{
	std::string value,tmp;

	itoa(*adcRegister,(char*)tmp.c_str(),10);
	value = tmp.c_str();
	return value;
}

std::string Battery::getPercentageString()
{
	std::string value,tmp;

	itoa((*adcRegister)/DIVIDER,(char*)tmp.c_str(),10);
	value = tmp.c_str();
	return value;
}

