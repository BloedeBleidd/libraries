/*
 * battery.h
 *
 *  Created on: 17.01.2017
 *      Author: BloedeBleidd
 */

#ifndef TANK_BATTERY_BATTERY_H_
#define TANK_BATTERY_BATTERY_H_

#include "stm32f10x.h"
#include <string>

const uint16_t DIVIDER = 41;

class Battery
{
private:
	uint16_t battery;
	uint16_t *adcRegister;

public:
	Battery(uint16_t *);

	uint16_t getValue();
	std::string getVoltageString();
	std::string getPercentageString();
};


#endif /* TANK_BATTERY_BATTERY_H_ */
